# backwardtuning

Recurrent network weight tuning

@aideAPI

### This only an alpha version, better contact us to discuss, what it is and how to use it :)

 We propose here a re-implementation of backward tuning as developped and descrined in [(vieville et al 2017)](https://inria.hal.science/INRIA-RRRT/hal-01610735v1) allowing to adjust the parameters of a vectorial recurrent discrete-time dynamical system.
 
 Demonstrations:
 - [Visualisation of a quadlin sigmoid transform adjustment on random output](./sigmoidtransformdemo/master.html)
 - [Visualisation of a quadlin sigmoid transform adjustment on a mirror transform](./sigmoidtransformdemo/servant.html)
 - [Visualisation of a quadlin sigmoid transform adjustment with hidden units](./sigmoidtransformdemo/sigmoid2.html)
 - [Visualisation of a simple reservoir transform simulation](./reservoirtransformdemo/dummy_task.html)
 
 
@slides backwardtuning.pdf

## Implementation class hierarchy 

![implementation class hierarchy](./class-hierarchy.png)


<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/backwardtuning'>https://gitlab.inria.fr/line/aide-group/backwardtuning</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/backwardtuning'>https://line.gitlabpages.inria.fr/aide-group/backwardtuning</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/backwardtuning/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/backwardtuning/-/tree/master/src</a>
- Version `0.1.0`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/backwardtuning.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>aidesys: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidesys'>Basic system C/C++ interface routines to ease multi-language middleware integration</a></tt>
- <tt>wjson: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/wjson'>Implements a JavaScript JSON weak-syntax reader and writer</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Author

- Thierry Viéville <thierry.vieville@inria.fr>

