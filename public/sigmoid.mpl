assume(w :: real, epsilon > 0, 1 > epsilon):

s := w -> 1 / (1 + exp(-4*w));

ds := unapply(simplify(D(s)(w)), w);

ok := limit(s(w),w=-infinity) = 0 and limit(s(w), w=+infinity) = 1 and ds(0) = 1;

w_max := 1/4 * log(1 / epsilon - 1); w_min := -w_max:

ok := simplify(w_max - solve(s(w)=1-epsilon, w)) = 0 and simplify(w_min - solve(s(w)=epsilon, w)) = 0;

solve({a * w_max + b = N - 1, a * w_min + b = 0}, {a, b});

N := unapply(round(w_max / epsilon), epsilon): # since 1/2 epsilon / delta ~= f'(x) < 1 and N = 2 w_max / delta

N(0.5);
N(1e-1);
N(1e-3);
N(1e-4);
N(1e-5);
N(1e-6);

#plot(N(epsilon), epsilon = 1e-6..1e-3);
