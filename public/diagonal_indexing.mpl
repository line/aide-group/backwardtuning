#
# Defines a diagonal 1D indexing of two indexes (n, t)
# 

# The indexing function k = k0(t, n) is in one to one correspondace with (t, n)

k0 := (t, n) -> (n + t) * (n + t + 1) / 2 + t:

# Showing the 1st values

Array(0..5, 0..5, k0);

# Computing the reciprocal function

k1 := proc(k)
 local m, t, n:
 # Here solving({m * (m + 1) / 2 = h}, {m})
 #  with { m = n + t >= t, k = h + t >= h }
 #  considering the greatest integer less than the m root
 m := floor(evalf((sqrt(1 + 8 * k) - 1) / 2)):
 t := k - m * (m + 1) / 2:
 n := m - t:
 # Checks the one to one correspondance
 if k <> k0(t, n) then print("Ahhh a bug: (k =", k, ") <> (k0(t, n) = ", k0(t, n), ")") fi:
 {'t' = evalf(t), 'n' = evalf(n)}
end:

# Verifies for a few values

for k from 0 to 100 do k1(k) od:

# Computes the values overload with respect to a 2D indexing
# for t < T, n < N => k0(T - 1, N - 1) is the highest value
# since k0 is increasing with t and n

k_more := unapply(simplify(1 + k0(T - 1, N - 1) - T * N), T, N);



 
