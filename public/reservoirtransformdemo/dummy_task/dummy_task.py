# Python 3.11
import json

import numpy as np # 1.26.1
import matplotlib.pyplot as plt # v3.1.1
import reservoirpy as rpy # v0.3.11

from reservoirpy.datasets import mackey_glass, to_forecasting
from reservoirpy.nodes import Reservoir, Ridge
from reservoirpy.mat_gen import normal
from reservoirpy.observables import rmse
rpy.set_seed(260_418)


# Dataset
series = mackey_glass(n_timesteps=10_000, seed=260_418)
X_train, X_test, Y_train, Y_test = to_forecasting(timeseries=series, forecast=50, test_size=0.2)


# Model
def sigmoid(x):
    return 1. / (1 + np.exp(-4 * x))

reservoir = Reservoir(
    units=30, 
    W=normal(degree=10), 
    Win=normal(degree=10), 
    input_bias=False, 
    activation=sigmoid,
    lr=0.1,
    sr=1.,
    seed=64, 
)
readout = Ridge(ridge=1e-7, input_bias=False)
model = reservoir >> readout


# Model training
model.fit(X=X_train, Y=Y_train)


# Running the model
Y_pred = model.run(X_test)
print(f"RMSE(Y_test, Y_pred) = ", rmse(y_true=Y_test, y_pred=Y_pred))
# RMSE(Y_test, Y_pred) =  0.048086203092155416


# Plot predicted vs expected
plt.figure()
plt.plot(Y_pred, 'r', label="Predicted output")
plt.plot(Y_test, 'b--', label="Expected output")
plt.xlabel("Timesteps")
plt.legend()
plt.title("Simple Mackey-Glass task results")
plt.show()

# Save everything nicely for Thierry

with open("./dummy_task/dataset.json", "w+") as dataset_file:
    json.dump({
        "X_train": X_train.T.tolist(), 
        "Y_train": Y_train.T.tolist(), 
        "X_test": X_test.T.tolist(), 
        "Y_test": Y_test.T.tolist(), 
        "Y_pred": Y_pred.T.tolist()
    }, dataset_file)

with open("./dummy_task/weights.json", "w+") as weights_file:
    json.dump({
        "W": reservoir.W.toarray().tolist(), 
        "Win": reservoir.Win.toarray().tolist(), 
        "Wout": readout.Wout.T.tolist()
    }, weights_file)