# dummy_task-forward-unit-slicesgnuplot display script (automatically generated, do NOT edit)
title="title \"`basename $0 | sed 's/.gnuplot.sh$//'`\""
while [ \! -z "$1" ] ; do case "$1" in
 --png)  line1='set term png'; line2='set output "dummy_task-forward-unit-slices.png"';;
 *) echo '$0 [--png]'; exit;;
esac; shift; done

cat << EOD | gnuplot -persist
$line1
$line2
plot "dummy_task-forward-unit-slices.dat" using 1:2:3 with line linecolor "green" notitle , "dummy_task-forward-unit-slices.dat" using 1:2:3 with yerrorbars linecolor "red" notitle
EOD
