# sigmoid2-forward-histogramgnuplot display script (automatically generated, do NOT edit)
title="title \"`basename $0 | sed 's/.gnuplot.sh$//'`\""
while [ \! -z "$1" ] ; do case "$1" in
 --png)  line1='set term png'; line2='set output "sigmoid2-forward-histogram.png"';;
 *) echo '$0 [--png]'; exit;;
esac; shift; done

cat << EOD | gnuplot -persist
$line1
$line2
set title "sigmoid2-forward-histogram as uniform"
set yrange [0:*] writeback
plot "sigmoid2-forward-histogram.dat" using 1:2 with boxes linecolor "black" notitle, "sigmoid2-forward-histogram.dat" using 1:3 with lines linecolor "red" linewidth 2 notitle

EOD
