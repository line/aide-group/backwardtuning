# master-backward-unit-slicesgnuplot display script (automatically generated, do NOT edit)
title="title \"`basename $0 | sed 's/.gnuplot.sh$//'`\""
while [ \! -z "$1" ] ; do case "$1" in
 --png)  line1='set term png'; line2='set output "master-backward-unit-slices.png"';;
 *) echo '$0 [--png]'; exit;;
esac; shift; done

cat << EOD | gnuplot -persist
$line1
$line2
plot "master-backward-unit-slices.dat" using 1:2:3 with line linecolor "green" notitle , "master-backward-unit-slices.dat" using 1:2:3 with yerrorbars linecolor "red" notitle
EOD
