#
# Generalized entropy elements
#

s      := sum(f(x[i]), i = 1 .. N):

rho    := -sum(f(x[i])/s * log(f(x[i])/s), i = 1 .. N):

drho_1 := -D(f)(x[1])/s * (rho + log(f(x[1])/s)):

# plot(p * log(p), p = 0..1);

# Checks the generalized entropy derivative

zero   := simplify(drho_1 - convert(diff(rho, x[1]), D)) assuming N = 16;

# Evaluates for f -> constant

rho_c := eval(subs(f = (x -> c), rho));

drho_1_c := diff(eval(subs(f = (x -> c), rho)), x[1]);

# Evaluates for f identity

s1   := eval(subs(f = (x -> x), s)):
rho1 := eval(subs(f = (x -> x), rho)):

drho_1_1 := -(rho1 + log(x[1]/s1)) / s1:

zero := simplify(drho_1_1 - diff(rho1, x[1])) assuming N = 4;

drho_2_1 := (1 + 2 * rho1 + (log(x[1]/s1) + log(x[2]/s1))) / s1^2:

zero := simplify(drho_2_1 - diff(rho1, x[1], x[2])) assuming N = 4;

# Evaluate the entropy 1st order variation between two histogram bins

series((p1-e) * log(p1-e) + (p2+e) * log(p2+e), e, 3);

