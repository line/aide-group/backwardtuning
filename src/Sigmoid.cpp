#include "Sigmoid.hpp"
#include "std.hpp"
#include "stats.hpp"
#include <math.h>

namespace backwardtuning {
  Sigmoid::Sigmoid(double epsilon)
  {
    // See https://gitlab.inria.fr/line/aide-group/backwardtuning/-/raw/master/src/sigmoid.mpl
    aidesys::alert(!(1e-6 <= epsilon && epsilon < 0.5), "illegal-argument", "in backwardtuning::Sigmoid::Sigmoid wrong epsilon: %e", epsilon);
    N = rint(0.5 * log(1 / epsilon - 1) / epsilon);
    // Tabulation buffers
    values = new double[N];
    dvalues = new double[N];
    {
      // Index calculation parameters
      a = 2 * (N - 1) / log(1 / epsilon - 1);
      b = (N - 1) / 2;
      // Buffer calculation
      for(unsigned int n = 0; n < N; n++) {
        double w = (n - b) / a, e = exp(-4 * w);
        values[n] = 1 / (1 + e);
        dvalues[n] = 4 * e / (1 + e) / (1 + e);
      }
    }
    // Max error calculation and verification
    {
      double e_max = 0;
      for(unsigned int n = 0; n < N; n++) {
        double w = (n + 0.5 - b) / a, e = exp(-4 * w);
        double e_f = fabs(f(w) - 1 / (1 + e));
        e_max = e_max < e_f ? e_f : e_max;
        double e_df = fabs(df(w) - 4 * e / (1 + e) / (1 + e));
        e_max = e_max < e_df ? e_df : e_max;
      }
      aidesys::alert(e_max > epsilon, "illegal-state", "in backwardtuning::Sigmoid::Sigmoid spurious maximal error: %e > epsilon: %e, N: %d", e_max, epsilon, N);
    }
  }
  Sigmoid::~Sigmoid()
  {
    delete[] values;
    delete[] dvalues;
  }
}
