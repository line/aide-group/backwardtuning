#include "LpOutputWeightAdjustment.hpp"
#include "std.hpp"

namespace backwardtuning {
  LpOutputWeightAdjustment::LpOutputWeightAdjustment(JSON parameters) : Transform(parameters), WeightAdjustment(parameters), OutputWeightAdjustment(parameters)
  {
    p = parameters.get("p", 2.0);
    aidesys::alert(!(1 < p && p <= 2), "illegal-argument", "in backwardtuning::LpOutputWeightAdjustment::setParameters p = %g must be in ]1, 2]", p);
    p1 = p - 1;
  }
  std::string LpOutputWeightAdjustment::asString() const
  {
    return aidesys::echo("LpOutputWeightAdjustment: { " + OutputWeightAdjustment::asString() + "p: %g }", p1 + 1);
  }
  double LpOutputWeightAdjustment::c() const
  {
    double r = 0;
    for(unsigned int t = 0; t < T; t++) {
      for(unsigned int n = 0; n < No; n++) {
        r += pow(getF(n, t) - o(n, t), p);
      }
    }
    return r / p;
  }
}
