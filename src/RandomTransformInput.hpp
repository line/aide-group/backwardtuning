#ifndef __backwardtuning_RandomTransformInput__
#define __backwardtuning_RandomTransformInput__

#include "TransformInput.hpp"

namespace backwardtuning {
  /**
   * @class RandomTransformInput
   * @description Defines a random input signal.
   * - The value is drawn from a normal distribution (of zero mean and unary standard-deviation).
   * - The corresponding `i(·)` [TransformInput](./TransformInput.html) function is implemented.
   * @extends TransformInput
   * @param {JSON} parameters Object parameter, including parent class parameters:
   *  - `unique [= false]` If true returns the same values for all instances of the class.
   */
  class RandomTransformInput: virtual public TransformInput {
    bool unique;
    const RandomTransformInput *input = NULL;
public:
    RandomTransformInput(const RandomTransformInput& transform) = delete;
    void operator = (const RandomTransformInput&) = delete;
    RandomTransformInput(JSON parameters);
    virtual ~RandomTransformInput() {}
    virtual std::string asString() const;
    virtual double i(unsigned int n, unsigned int t) const
    {
      return input == this ? TransformInput::i(n, t) : input->i(n, t);
    }
  };
}
#endif
