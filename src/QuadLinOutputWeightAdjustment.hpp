#ifndef __backwardtuning_QuadLinOutputWeightAdjustment__
#define __backwardtuning_QuadLinOutputWeightAdjustment__

#include "OutputWeightAdjustment.hpp"
#include "QuadLin.hpp"

namespace backwardtuning {
  /**
   * @class QuadLinOutputWeightAdjustment
   * @description Defines a input/output dynamical system tuned by backward learning.
   * - It implements the [c(·)](./OutputWeightAdjustment.html#c) and [dc(·)](./OutputWeightAdjustment.html#dc) method of the cost function.
   *    - `c = sum_{nt} q_(w_o)(o_n(t) - x_n(t))` where `q_(w_o)(·)` is the [quadlin](./quadlin.pdf) profile rather quadratic below ``w_o´´ and linear above it, with `0 <= c <= N T M^2 / (w_0 + M)`, `|x_n(t)| <= M`.
   * @extends OutputWeightAdjustment
   * @param {JSON} parameters Object parameter, including parent class parameters:
   * - `w_o [= 1.0] The criterion is a [quadlin](./quadlin.pdf) of parameter `w_o`.
   */
  class QuadLinOutputWeightAdjustment: virtual public OutputWeightAdjustment {
    double w_o;
    QuadLin q;
public:
    QuadLinOutputWeightAdjustment(const QuadLinOutputWeightAdjustment& transform) = delete;
    void operator = (const QuadLinOutputWeightAdjustment&) = delete;
    QuadLinOutputWeightAdjustment(JSON parameters);
    virtual ~QuadLinOutputWeightAdjustment() {}
    virtual std::string asString() const;

    virtual double c() const;
    virtual double dc(unsigned int m, unsigned int s) const
    {
      return s < T && m < No ? q.df(getF(m, s) - o(m, s)) : 0;
    }
  };
}
#endif
