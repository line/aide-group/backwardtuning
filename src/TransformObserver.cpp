#include "TransformObserver.hpp"
#include "std.hpp"
#include <math.h>
#include "file.hpp"
#include "regex.hpp"

namespace backwardtuning {
  void TransformObserver::add(String c, const Transform& transform)
  {
    for(unsigned int t = 0; t < transform.T; t++) {
      for(unsigned int n = 0; n < transform.N; n++) {
        TransformBuffer::add(c + "-forward", n, t, transform.getF(n, t));
      }
    }
  }
  void TransformObserver::add(String c, const AdjustableTransform& transform)
  {
    add(c, (const Transform&) transform);
    for(unsigned int t = 0; t < transform.T; t++) {
      for(unsigned int n = 0; n < transform.N; n++) {
        TransformBuffer::add(c + "-backward", n, t, transform.getB(n, t));
      }
    }
  }
  std::string TransformObserver::getStat(String c, unsigned int mask) const
  {
    return "{\n\tchannel: " + c + aidesys::getStat(getSamples(c), NULL, 0, mask).substr(1);
  }
  const std::vector < std::string > TransformObserver::getStats(String c, char what, unsigned int mask) const {
    std::vector < std::string > result;
    switch(what) {
    case 't':
    {
      for(unsigned int t = 0; t < Tsup.at(c); t++) {
        result.push_back("{\n\tchannel: " + c + aidesys::echo("\n\tt: %d", t) + aidesys::getStat(getTimeSlice(c, t), NULL, 0, mask).substr(1));
      }
      return result;
    }
    case 'u':
    {
      for(unsigned int n = 0; n < Nsup.at(c); n++) {
        result.push_back("{\n\tchannel: " + c + aidesys::echo("\n\tn: %d", n) + aidesys::getStat(getUnitSlice(c, n), NULL, 0, mask).substr(1));
      }
      return result;
    }
    default:
    {
      aidesys::alert(true, "illegal-argument", "in backwardtuning::TransformObserver::getStats what: '%c' not in {'t', 'u'}", what);
      return result;
    }
    }
  }
  void TransformObserver::plotData(String c) const
  {
    const std::vector < double >& data = getSamples(c);
    unsigned int histo[data.size()];
    std::string stat = aidesys::getStat(data, histo);
    aidesys::save(c + "-stats.json", "{\n\tchannel: " + c + stat.substr(1));
    aidesys::plotHistogram(c + "-histogram", stat, histo, true);
    plotStatCurve(c, 'u');
    plotStatCurve(c, 't');
  }
  void TransformObserver::plotStatCurve(String c, char what)  const
  {
    const std::vector < std::string >& stats = getStats(c, what, 0x01);
    aidesys::plotStatCurve(c + (what == 't' ? "-time-slices" : "-unit-slices"), stats, 0, what == 't' ? Tsup.at(c) : Nsup.at(c));
  }
  void TransformObserver::simulate(String c, AdjustableTransform& transform, unsigned int count, double epsilon)
  {
    transform.adjust(count, epsilon);
    add(c, transform);
    aidesys::save(c + "-parameters.json", "{\n simulation-name: '" + c + "' " + transform.asString() + "}\n");
    aidesys::plotStatCurve(c + "-costs", transform.costs);
    // Weights histogram
    {
      unsigned int K = transform.getParameters().get("K", 0);
      unsigned int histo[K];
      std::string stat = aidesys::getStat(transform.getWeights(), K, histo);
      aidesys::plotHistogram(c + "-weights-histogram", stat, histo, true);
    }
    aidesys::plotStatCurve(c + "-upsilons", transform.upsilons);
    plotData(c + "-forward");
    plotData(c + "-backward");
  }
  double TransformObserver::getSpatialDivergence(String c1, String c2, char what) const
  {
    switch(what) {
    case 'g':
    {
      std::vector < double > all1 = getSamples(c1), all2 = getSamples(c2);
      return aidesys::getDivergence(all1, all2);
    }
    case 't':
    {
      double r = 0;
      for(unsigned int t = 0; t < std::min(Tsup.at(c1), Tsup.at(c2)); t++) {
        r += aidesys::getDivergence(getTimeSlice(c1, t), getTimeSlice(c2, t));
      }
      return r;
    }
    case 'u':
    {
      double r = 0;
      for(unsigned int n = 0; n < std::min(Nsup.at(c1), Nsup.at(c2)); n++) {
        r += aidesys::getDivergence(getUnitSlice(c1, n), getUnitSlice(c2, n));
      }
      return r;
    }
    default:
      aidesys::alert(true, "illegal-argument", "in backwardtuning::TransformObserver::getSpatialDivergence what: '%c' not in {'g', 't', 'u'}", what);
      return 0;
    }
  }
  std::string TransformObserver::asString() const
  {
    return "{ TransformObserver: {" + TransformBuffer::asString() + "\n  } \n}";
  }
}
