#include "RandomTransformInput.hpp"
#include "random.hpp"
#include <map>

namespace backwardtuning {
  // Implements static random inputs
  class RandomInputs {
    std::map < std::pair < unsigned int, unsigned int >, RandomTransformInput * > inputs;
public:
    const RandomTransformInput *get(JSON parameters)
    {
      auto ik = std::pair < unsigned int, unsigned int > (parameters.get("Ni", 0), parameters.get("T", 0));
      auto it = inputs.find(ik);
      if(it == inputs.cend()) {
        RandomTransformInput *input = new RandomTransformInput(parameters);
        it = inputs.insert(std::pair < std::pair < unsigned int, unsigned int >, RandomTransformInput * > (ik, input)).first;
      }
      return it->second;
    }
    ~RandomInputs() {
      for(auto it = inputs.begin(); it != inputs.cend(); it++) {
        delete it->second;
      }
    }
  }
  randomInputs;
  RandomTransformInput::RandomTransformInput(JSON parameters) : Transform(parameters), TransformInput(parameters)
  {
    unique = parameters.get("unique", false);
    if(unique) {
      input = randomInputs.get(parameters.clone().set("unique", false));
    } else {
      setValues([] (unsigned int n, unsigned int t) {
        return aidesys::random('n');
      }, Ni, T);
      input = this;
    }
  }
  std::string RandomTransformInput::asString() const
  {
    return "RandomTransformInput: { " + TransformInput::asString() + " unique: " + (unique ? "true" : "false") + " }";
  }
}
