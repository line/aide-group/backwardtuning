#ifndef __backwardtuning_TransformObserver__
#define __backwardtuning_TransformObserver__

#include <map>
#include <vector>
#include <set>
#include "stats.hpp"
#include "TransformBuffer.hpp"
#include "Transform.hpp"
#include "AdjustableTransform.hpp"

namespace backwardtuning {
  /**
   * @class TransformObserver
   * @description Defines a transform data buffer with some statistics estimation.
   * - The data is structured in terms of named ``channel´´ storing spatio-temporal values indexed by `(n, t)`.
   */
  class TransformObserver: public TransformBuffer {
public:

    /**
     * @function add
     * @memberof TransformObserver
     * @instance
     * @description Adds the forward values of Transform, with the backward values if an AdjustableTransform.
     * - Considering the transform name `$c`:
     *    - Forward  values are stored in the `$c-forward` channel.
     *    - Backward values are stored in the `$c-backward` channel.
     * @param {string} c The transform name.
     * @param {Transform} transform The transform to consider.
     */
    void add(String c, const Transform& transform);
    void add(String c, const AdjustableTransform& transform);

    /**
     * @function getStat
     * @memberof TransformObserver
     * @instance
     * @description Returns one channel statistics.
     * - See [getStat(·)](https://line.gitlabpages.inria.fr/aide-group/aidesys/stats.html#.getStat) for details.
     * @param {string} c The buffer channel name.
     * @param {uint} [mask = 0xF] An optional mask to deselect some calculation:
     * - _0X1_ As a function of the sample momenta.
     * - _0X2_ As a function of the sample momenta, with respect to usual distributions.
     * - _0X4_ As a function of the histogram.
     * - _0X8_ As a function of the histogram, with respect to usual distributions.
     * @return {Array} A `std::vector < std::string >` vector of statistics, indexed by time 't' if considering unit slices, indexed by unit index 'n' if considering time slices. => @TBD A REVOIR
     */
    std::string getStat(String c, unsigned int mask = 0xF) const;

    /**
     * @function getStats
     * @memberof TransformObserver
     * @instance
     * @description Returns the channel or the time-slice statistics.
     * - See [getStat(·)](https://line.gitlabpages.inria.fr/aide-group/aidesys/stats.html#.getStat) for details.
     * @param {string} c The buffer channel name.
     * @param {char} what Either
     * - 't' for "time-stat", considering temporal statistics indexed by `0 <= t < T`, on time-slices;
     * - 'u' for "unit-stats", considering spatial statistics, indexed by `0 <= n < N`, on unit-slices.
     * @param {uint} [mask = 0xF] An optional mask to deselect some calculation:
     * - _0X1_ As a function of the sample momenta.
     * - _0X2_ As a function of the sample momenta, with respect to usual distributions.
     * - _0X4_ As a function of the histogram.
     * - _0X8_ As a function of the histogram, with respect to usual distributions.
     * @return {Array} A `std::vector < std::string >` vector of statistics, either
     * - indexed by time 't' if considering time-stats, or
     * - indexed by unit index 'n' if considering unit-stats.
     */
    const std::vector < std::string > getStats(String c, char what, unsigned int mask = 0xF) const;

    /**
     * @function plotData
     * @memberof TransformObserver
     * @instance
     * @description Saves the the channel data.
     * - Stores the statistics parameters obtained by [getStat(·)](https://line.gitlabpages.inria.fr/aide-group/aidesys/stats.html#.getStat) in a file of name `$c.json`,
     * - Stores the related histogram via [plotHistogram(·)](https://line.gitlabpages.inria.fr/aide-group/aidesys/stats.html#.plotHistogram) in `$c(.png|.dat|.gnuplot.sh).
     *  - Stores the temporal and spatial statitics curves using [plotStats(·)](https://line.gitlabpages.inria.fr/aide-group/aidesys/stats.html#.plotStats), in `$c(.png|.dat|.gnuplot.sh) files.
     *  ,    - The gnu plots use [gnuplot](https://line.gitlabpages.inria.fr/aide-group/aidesys/file.html#.gnuplot).
     * @param {string} c The buffer channel name.
     */
    void plotData(String c) const;
private:
    void plotStatCurve(String c, char what) const;
public:

    /**
     * @function simulate
     * @memberof TransformObserver
     * @instance
     * @description Simulates the forward/backward/weight adjustment of an adjustable transform.
     *  - Plots the related data using [plotData](#PlotData).
     * @param {string} c The simulation name.
     * @param {AdjustableTransform} transform The adjustable transform to consider.
     * @param {uint} [count=1] The number of adjustment steps. Set to `-1` for an almost unbounded number of steps.
     * @param {double} [epsilon=0] The minimal cost decrease for an adjustment to be valid. Set to `0` not skip the test.
     */
    void simulate(String c, AdjustableTransform& transform, unsigned int count = 1, double epsilon = 0);

    /**
     * @function getSpatialDivergence
     * @memberof TransformObserver
     * @instance
     * @description Calculates the Kullback-Leibler divergence between between two channels.
     * @param {String} c1 The empirical data channel.
     * @param {String} c2 The reference (or model) data channel.
     * @param {char} what Either:
     * - 'g' for "global" for considering the whole distribution, without spatial or temporal distinction;
     * - 't' for "unit-stats", thus computes the divergence instant by instant;
     * - 'u' for "unit-stats", thus computes the divergence unit by unit.
     * @return The Kullback-Leibler divergence, i.e., the average number of bits difference when coding `c1` with `c2`.
     */
    double getSpatialDivergence(String c1, String c2, char what) const;

    /**
     * @function asString
     * @memberof TransformObserver
     * @instance
     * @description Returns object parameter as a parsable weak json string.
     * @return {string} All channels unit and time counts.
     */
    std::string asString() const;
  };
}
#endif
