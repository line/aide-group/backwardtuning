#include "NLNTransform.hpp"
#include <stdlib.h>
#include <algorithm>

namespace backwardtuning {
  Sigmoid NLNTransform::sigmoid;
  //
  NLNTransform::NLNTransform(JSON parameters) : Transform(parameters), WeightAdjustment(parameters), WeightRegularization(parameters), AdjustableTransform(parameters), TransformInput(parameters)
  {
    Nc = N + Ni;
    leak = parameters.get("leak", 0.0), uleak = 1 - leak;
    aidesys::alert(K > N * Nc - N, "illegal-argument", "in NLNTransform::setParameters more parameters K: %d than possible connections ((N: %d) x (N + Ni: %d) - N): %d", K, N, Ni, N * Nc - N);
    unit_connections.resize(N);
  }
  NLNTransform::NLNTransform(const NLNTransform& transform) : Transform(transform.parameters), WeightAdjustment(transform.parameters), WeightRegularization(transform.parameters), AdjustableTransform(transform), TransformInput(transform.parameters), leak(transform.leak), uleak(transform.uleak), Nc(transform.Nc), unit_connections(transform.unit_connections), network_connection_weights(transform.network_connection_weights), network_connection_units(transform.network_connection_units)
  {}
  void NLNTransform::init(wjson::Value& parameters)
  {
    parameters.set("R", 1);
    if(K == N * Nc - N) {
      for(unsigned int k = 0; k < K; k++) {
        unsigned int n = k % N, m_ = k / N, m = m_ < n ? n : m_ + 1;
        createConnection(n, m, k);
      }
    } else {
      for(unsigned int k = 0; k < K; k++) {
        // Draws a random unit pair and iterates to find a new unit pair
        for(unsigned int l = 0, nm = random() % (N * Nc); l < N * Nc; l++, nm = (nm + 1) % (N * Nc)) {
          unsigned int n = nm % N, m = nm / N;
          if(n != m && createConnection(n, m, k)) {
            break;
          }
        }
      }
    }
  }
  bool NLNTransform::createConnection(unsigned int n, unsigned int m, unsigned int k)
  {
    if(network_connection_weights.find(std::pair < unsigned int, unsigned int > (n, m)) != network_connection_weights.end()) {
      return false;
    }
    unit_connections[n].push_back(std::pair < unsigned int, unsigned int > (k, m));
    network_connection_weights[std::pair < unsigned int, unsigned int > (n, m)] = k;
    network_connection_units[std::pair < unsigned int, unsigned int > (n, k)] = m;
    return true;
  }
  std::string NLNTransform::asString() const
  {
    std::string s = "";
    for(auto it = network_connection_weights.cbegin(); it != network_connection_weights.cend(); it++) {
      s += aidesys::echo(" [ %d %d ]", it->first.first, it->first.second);
    }
    return aidesys::echo("NLNTransform: { " + AdjustableTransform::asString() + (s == "" ? "" : " connections: [" + s + " ]") + " leak: %g }", leak);
  }
}
