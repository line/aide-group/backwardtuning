#ifndef __backwardtuning_AdjustableTransform__
#define __backwardtuning_AdjustableTransform__

#include <math.h>
#include <array>
#include <iterator>
#include "Value.hpp"
#include "Transform.hpp"
#include "WeightRegularization.hpp"
#include "WeightAdjustment.hpp"

namespace backwardtuning {
  /**
   * @class AdjustableTransform
   * @abstract
   * @description Defines a dynamical system tuned by backward learning.
   * @slides adjustabletransform.pdf
   *
   * - The `df(·)` methods have to be implemented.
   * - The backward state values are calculated, updated if needed and buffered for `0 <= n < N` and `0 <= t < T`.
   * @extends Transform
   * @extends WeightAdjustment
   * @extends WeightRegularization
   * @param {JSON} parameters Object parameter, including parent class parameters:
   * - `K0` : Specifies the weights to be adjusted.
   *   - `0 < K0 <= K` allows to estimate only the first `K0` weights of index `0 <= k < K0`, while the `K-K0` weights remains fixed.
   *   - `0` corresponds to no weith adjustment.
   *   - otherwise all weights are adjusted.
   * - `R` : Maximal recurrence of time-window the system equation (used to bound backward tuning calculations).
   *   - `R = 1` means that the current value depends only on the previous values.
   *   - `R = 0` means that the current value depends on all previous values (default).
   */
  class AdjustableTransform: virtual public Transform, virtual public WeightAdjustment, virtual public WeightRegularization {
    friend class TransformBuffer;
    void initConnectivity();
    // AdjustableTransform buffers
    double *bvalues = NULL;
    // This flag is to be set to true by derived classes if state values or weights are updated
    bool bmodified = true;
    // Backward tuning, it is automatically called if required
    void backward();
    // AdjustableTransform dimensions
    unsigned int R = -1, K0 = -1;
    // Connectivity arrays
    bool fully_connected = true, global_weights = true;
    // The efferent-unit-index -> [(afferent-unit-index, unit-delay), …], n -> [(m, r), …] map
    std::vector < std::vector < std::pair < unsigned int, unsigned int >> > connections;
    // The weight-index -> [efferent-unit-index, ..], k -> [n, …] map
    std::vector < std::vector < unsigned int >> dependencies;
public:
    AdjustableTransform(JSON parameters);
    AdjustableTransform(const AdjustableTransform& transform);
    virtual ~AdjustableTransform();
    virtual std::string asString() const;

    virtual void setW(unsigned int k, double w)
    {
      bmodified = true;
      Transform::setW(k, w);
    }
    /**
     * @function isConnected
     * @memberof AdjustableTransform
     * @instance
     * @abstract
     * @description Checks if the spatio-temporal values are connected.
     *  - The default implementation considers a fully connected network, thus returns true.
     *  - This is to overleaded for sparse connected networks, in order to increase performances.
     *  - This method is called at the beggining of the use of this object only, thus no need to optimize it.
     *  - The network connectivity is assumed to be stationnary, if not, it must be implemented via zero value weight.
     * @param {uint} n Efferent unit index.
     * @param {uint} m Afferent uint index.
     * @param {uint} r Current time difference `t - s >= 0`.
     * @return {bool} True if the spatio-temporal value of index `(n, t)` depends on the one of index `(m, s = t - r)`.
     *  - To preserve causality if `t == s` we must have `n > m`.
     */
    virtual bool isConnected(unsigned int n, unsigned int m, unsigned int r) const;

    /**
     * @function isDependent
     * @memberof AdjustableTransform
     * @instance
     * @abstract
     * @description Checks if a unit value depends on a weight value.
     * - The default implementation considers the weight influence for the whole network.
     * - This is to overleaded to state local weight influence, in order to increase performances.
     *  - This method is called at the beggining of the use of this object only, thus no need to optimize it.
     * - The network weight dependency is assumed to be stationnary, if not, it must be implemented via zero value weight.
     * @param {uint} n Efferent unit index.
     * @param {uint} k The weight index.
     * @return {bool} True if the value of index `n` depends on the weight of index `k`.
     */
    virtual bool isDependent(unsigned int n, unsigned int k) const;

    /**
     * @function df
     * @memberof AdjustableTransform
     * @instance
     * @abstract
     * @description Implements the dynamic equation state derivative.
     * - Calculates `d_{ms} f_n(t, …)`.
     * - By contract, `df(m, s, n, t)` is called only if
     *   - `n < N`, `t < T`, `m < N`, `s < T`, `t - s <= R`,
     *   - units of indexes `(n, t)` and `(m, s)` are connected,
     *     - thus no need to re-check these conditions.
     * @param {uint} m Afferent unit index.
     * @param {uint} s Time derivative.
     * @param {uint} n Efferent unit index.
     * @param {uint} t Current time.
     * @return {double} The corresponding value.
     */
    virtual double df(unsigned int m, unsigned int s, unsigned int n, unsigned int t) const;

    /**
     * @function df
     * @memberof AdjustableTransform
     * @instance
     * @abstract
     * @description Implements the dynamic equation parameter derivative.
     * - Calculates `d_{W_k} f_n(t, …)`.
     * - By contract, `df(k, n, t)` is called only if
     *   - `k < K`, `n < N`, `t < T`,
     *   - unit of index `n` depends on weight of index `k`,
     *     - thus no need to re-check these conditions.
     * @param {uint} k Parameter index derivative.
     * @param {uint} n Unit index.
     * @param {uint} t Current time.
     * @return {double} The corresponding value.
     */
    virtual double df(unsigned int k, unsigned int n, unsigned int t) const;

    /**
     * @function getB
     * @memberof AdjustableTransform
     * @instance
     * @description Returns the current backward state value.
     * @param {uint} n Unit index.
     * @param {uint} t Current time.
     * @return {double} The corresponding value, or `NAN` if out of bound.
     */
    double getB(unsigned int n, unsigned int t) const
    {
      if(bmodified || fmodified) {
        const_cast < AdjustableTransform & > (*this).backward();
      }
      return N <= n || T <= t ? 0 : bvalues[n + N * t];
    }
    /**
     * @function getBValues
     * @memberof AdjustableTransform
     * @instance
     * @description Returns the current backward values.
     * @return {Array} A 1D `double[N * T]` array with `x_n(t) = values[n + N * t]`.
     */
    const double *getBValues() const
    {
      if(bmodified) {
        const_cast < AdjustableTransform & > (*this).backward();
      }
      return bvalues;
    }
    /**
     * @function adjust
     * @memberof AdjustableTransform
     * @instance
     * @description Performs one or more step of parameter adjustment.
     * - The network weights are changed in order to correspond to the minimal cost.
     * @param {uint} [count=1] The number of adjustment steps. Set to `-1` for an almost unbounded number of steps.
     * @param {double} [epsilon=0] The minimal weight adjustment gain `upsilon`. Set to `0` not skip the test.
     * @return {double} The obtained cost after the adjustment·s.
     * - Each obtained cost is also registered in the [costs](#costs) vector.
     */
    double adjust(unsigned int count = 1, double epsilon = 0);

    /**
     * @function adjustWeights
     * @memberof AdjustableTransform
     * @instance
     * @description Performs the adjustment of the weights, given forward and backward values.
     *  - This method can be overwritten for specific networks to increase convergence.
     *  - This methods directly uses the class members variables.
     */
    virtual void adjustWeights();

    /**
     * @member {Array} costs
     * @memberof AdjustableTransform
     * @instance
     * @description Trace of the costs decrease when calling adjust.
     * - Before the 1st step and after each adjustment done calling [adjust()](#adjust), the cost evaluation is pushed.
     * - Only decreasing costs (thus updating the weights) are taken into account.
     * - This `std::vector<double>` buffer can be edited, cleared, and so on.
     */
    std::vector < double > costs;

    /**
     * @member {Array} upsilons
     * @memberof AdjustableTransform
     * @instance
     * @description Trace of the upsilon adjustment when calling adjust.
     * - When calling [adjust()](#adjust), the upsilon value is pushed before each use.
     * - This `std::vector<double>` buffer can be edited, cleared, and so on.
     */
    std::vector < double > upsilons;
  };
}
#endif
