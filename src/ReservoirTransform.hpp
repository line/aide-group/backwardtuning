#ifndef __backwardtuning_ReservoirTransform__
#define __backwardtuning_ReservoirTransform__

#include "Sigmoid.hpp"
#include "BufferedOutputWeightAdjustment.hpp"
#include "NLNTransform.hpp"
#include "TransformBuffer.hpp"
#include <map>

namespace backwardtuning {
  /**
   * @class ReservoirTransform
   * @description Defines a input/output basic reservoir transform.
   * @slides reservoir-transform
   * - The [`ReservoirTransform::init`](#.init) function is to be used to pass the `parameters` to the constructor.
   * - It implements the [f(·)](./Transform.html#f), and [df(·)](./AdjustableTransform.html#df) functions.
   * @extends NLNTransform
   * @extends BufferedOutputWeightAdjustment
   * @param {JSON} parameters Object parameter, including parent class parameters:
   *  - `weights`: The file name containing the `Win`, `Wout` and `W` connection weights in wjson format.
   *  - `dataset`: The file name containing input/output (e.g., test or learning data) data.
   *    - `input`: The input data, stores in the `data` file: `Ni` arrays of `T` sequences of values, e.g., `X_test`.
   *    - `output`: The input data, stores in the `data` file: `No` arrays of `T` sequences of values, e.g., `Y_test`.
   *  - `adjust`: Defines which weights are to be adjusted, by default `adjust = 0x1 | 0x2 | 0x4 = 0x7` adjusting all weights.
   *    - `0x1`: output weights.
   *    - `0x2`: reservoir weights.
   *    - `0x4`: input weights.
   */
  class ReservoirTransform: public NLNTransform, public BufferedOutputWeightAdjustment {
    TransformBuffer buffer;
    unsigned int adjust;
protected:
    void init(wjson::Value& parameters);
public:
    ReservoirTransform(const ReservoirTransform& transform) = delete;
    void operator = (const ReservoirTransform&) = delete;
    ReservoirTransform(JSON parameters);
    virtual ~ReservoirTransform() {}
    virtual std::string asString() const;

    virtual double f(unsigned int n, unsigned int t) const
    {
      if(n < No) {
        return NLNTransform::g(n, t);
      }else {
        return NLNTransform::f(n, t);
      }
    }
    virtual double df(unsigned int m, unsigned int s, unsigned int n, unsigned int t) const
    {
      if(n < No) {
        return getW(n, m);
      }else {
        return NLNTransform::df(m, s, n, t);
      }
    }
    virtual double df(unsigned int k, unsigned int n, unsigned int t) const
    {
      if(n < No) {
        return getF(k, n, t);
      }else {
        return NLNTransform::df(k, n, t);
      }
    }
  };
}
#endif
