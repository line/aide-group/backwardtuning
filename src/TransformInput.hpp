#ifndef __backwardtuning_TransformInput__
#define __backwardtuning_TransformInput__

#include "Transform.hpp"
#include "Value.hpp"
#include <functional>

namespace backwardtuning {
  /**
   * @class TransformInput
   * @description Defines a transform input interface.
   * - The input `i(·)` is implemented.
   * @extends Transform
   * @param {JSON} parameters Object parameter:
   * - `Ni` Number of input units.
   */
  class TransformInput: virtual public Transform {
protected:
    bool uninitialized = true;
    double *values = NULL;
    unsigned int Ni;
public:
    TransformInput(const TransformInput& transform) = delete;
    void operator = (const TransformInput&) = delete;
    TransformInput(JSON parameters);
    virtual ~TransformInput();
    virtual std::string asString() const;

    /**
     * @function setValues
     * @memberof TransformInput
     * @instance
     * @description Sets the buffer values.
     * @param {Array|Function} values A 1D `double[Ni * T]` array with `i_n(t) = values[n + Ni * t]`, values are copied.
     * - It can also be provided as a `std::vector<double>` container.
     * - It can also be provided as a function or as a lambda: <br/>`transform.setValues([] (unsigned int n, unsigned int t) { return input(n, t); }`
     * @param {uint} Ni Number of input units.
     * @param {uint} T Number of time steps.
     */
    void setValues(const double *values, unsigned int Ni, unsigned int T);
    void setValues(const std::vector < double > &values, unsigned int Ni, unsigned int T);
    void setValues(std::function < double (unsigned int, unsigned int) > values, unsigned int Ni, unsigned int T);

    /**
     * @function i
     * @memberof TransformInput
     * @instance
     * @description Returns the current forward value.
     * @param {uint} n Input unit index.
     * @param {uint} t Current time.
     * @return {double} The corresponding value, or `NAN` if out of bound.
     */
    virtual double i(unsigned int n, unsigned int t) const
    {
      aidesys::alert(uninitialized, "illegal-state", "in backwardtuning::TransformInput::i(n,t) setValues() must be called before using i()");
      return Ni <= n || T <= t ? 0 : values[n + Ni * t];
    }
  };
}
#endif
