#ifndef __backwardtuning_OutputWeightAdjustment__
#define __backwardtuning_OutputWeightAdjustment__

#include "Value.hpp"
#include "WeightAdjustment.hpp"

namespace backwardtuning {
  /**
   * @class OutputWeightAdjustment
   * @description Defines the output adjustment interface.
   * - The `o(·)` desired output function is to be implemented.
   * @extends WeightAdjustment
   * @param {JSON} parameters Object parameter, including parent class parameters.
   */
  class OutputWeightAdjustment: virtual public WeightAdjustment {
public:
    OutputWeightAdjustment(const OutputWeightAdjustment& transform) = delete;
    void operator = (const OutputWeightAdjustment&) = delete;
    OutputWeightAdjustment(JSON parameters);
    virtual ~OutputWeightAdjustment() {}
    virtual std::string asString() const;

    /**
     * @function o
     * @memberof OutputWeightAdjustment
     * @instance
     * @abstract
     * @description Returns the desired output value.
     * @param {uint} n Input unit index.
     * @param {uint} t Current time.
     * @return {double} The corresponding value, or `NAN` if out of bound.
     */
    virtual double o(unsigned int n, unsigned int t) const;
  };
}
#endif
