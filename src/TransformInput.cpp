#include "TransformInput.hpp"
#include <math.h>
#include "std.hpp"

namespace backwardtuning {
  TransformInput::TransformInput(JSON parameters) : Transform(parameters)
  {
    Ni = parameters.get("Ni", 0);
    aidesys::alert(Ni == 0, "illegal-argument", "in backwardtuning::TransformInput::setParameters Ni: 0");
    values = new double[Ni * T];
  }
  TransformInput::~TransformInput()
  {
    delete[] values;
  }
  void TransformInput::setValues(const double *values, unsigned int Ni, unsigned int T)
  {
    setValues([values, Ni](unsigned int n, unsigned int t) { return values[n + Ni * t];
              }, Ni, T);
  }
  void TransformInput::setValues(const std::vector < double > &values, unsigned int Ni, unsigned int T)
  {
    setValues([values, Ni](unsigned int n, unsigned int t) { return values.at(n + Ni * t);
              }, Ni, T);
  }
  void TransformInput::setValues(std::function < double (unsigned int, unsigned int) > values_, unsigned int Ni, unsigned int T)
  {
    aidesys::alert(values == NULL, "illegal-state", "in backwardtuning::TransformInput::setValues setParameters() must be called before using setValues");
    aidesys::alert(this->Ni != Ni || this->T != T, "illegal-argument", "in backwardtuning::TransformInput::setValues this input buffer[Ni:d x T:%d] sizes are incoherent with the values[Ni:d x T:%d] sizes", this->Ni, this->T, Ni, T);
    uninitialized = false;
    for(unsigned int t = 0, nt = 0; t < T; t++) {
      for(unsigned int n = 0; n < Ni; n++, nt++) {
        values[nt] = values_(n, t);
      }
    }
  }
  std::string TransformInput::asString() const
  {
    return aidesys::echo("TransformInput: { Ni: %d }", Ni);
  }
}
