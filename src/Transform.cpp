#include "Transform.hpp"
#include "random.hpp"
#include "std.hpp"
 #include <string.h>

namespace backwardtuning {
  Transform::Transform(JSON parameters)
  {
    init(this->parameters = parameters);
    N = parameters.get("N", 0);
    T = parameters.get("T", 0);
    K = parameters.get("K", 0);
    aidesys::alert(N == 0 || T == 0 || K == 0, "illegal-argument", "in backwardtuning::Transform::setParameters {N: %d T: %d K:%d} parameters must be strictly positive", N, T, K);
    fvalues = new double[N * T];
    for(unsigned int nt = 0; nt < N * T; fvalues[nt++] = NAN) {}
    weights = new double[K];
    setWeights(parameters.get("Wstdev", 0.0));
  }
  Transform::Transform(const Transform& transform) : parameters(transform.parameters), fmodified(true), N(transform.N), T(transform.T), K(transform.K)
  {
    fvalues = new double[N * T];
    for(unsigned int nt = 0; nt < N * T; fvalues[nt++] = NAN) {}
    weights = new double[K];
    memcpy(weights, transform.weights, K * sizeof(double));
  }
  Transform::~Transform()
  {
    delete[] fvalues;
    delete[] weights;
  }
  std::string Transform::asString() const
  {
    return aidesys::echo("Transform: { N: %d T: %d K: %d Wstdev: %g}", N, T, K, parameters.get("Wstdev", 0.0));
  }
  double Transform::f(unsigned int n, unsigned int t) const
  {
    aidesys::alert(true, "illegal-state", "in backwardtuning::Transform::f(n,t), this virtual method must be overridden");
    return NAN;
  }
  void Transform::setWeights(double Wstdev)
  {
    Wstdev = Wstdev == -1 ? 1.0 / sqrt(N) : Wstdev == -2 ? 1.0 / K : Wstdev < 0 ? 0 : Wstdev;
    for(unsigned int k = 0; k < K; k++) {
      setW(k, Wstdev * aidesys::random('n'));
    }
  }
  void Transform::setWeights(const double *W)
  {
    for(unsigned int k = 0; k < K; k++) {
      setW(k, W[k]);
    }
  }
  void Transform::setWeights(const Transform& source)
  {
    unsigned int K0 = K < source.K ? K : source.K;
    for(unsigned int k = 0; k < K0; k++) {
      setW(k, source.getW(k));
    }
  }
  double Transform::getWeightsDistance(const Transform& source, char norm, bool relative) const
  {
    unsigned int K0 = K < source.K ? K : source.K;
    switch(norm) {
    case 0:
    {
      double r = 0;
      for(unsigned k = 0; k < K0; k++) {
        double e = fabs(getW(k) - source.getW(k));
        if(e < r) {
          r = e;
        }
      }
      if(relative) {
        double d = 0;
        for(unsigned k = 0; k < K0; k++) {
          double e = fabs(source.getW(k));
          if(e < d) {
            d = e;
          }
        }
        r /= d;
      }
      return r;
    }
    case 1:
    {
      double r = 0;
      for(unsigned k = 0; k < K0; k++) {
        r += fabs(getW(k) - source.getW(k));
      }
      if(relative) {
        double d = 0;
        for(unsigned k = 0; k < K0; k++) {
          d += fabs(source.getW(k));
        }
        r /= d;
      }
      return r;
    }
    default:
    {
      double r = 0;
      for(unsigned k = 0; k < K0; k++) {
        double e = getW(k) - source.getW(k);
        r += e * e;
      }
      if(relative) {
        double d = 0;
        for(unsigned k = 0; k < K0; k++) {
          double e = source.getW(k);
          d += e * e;
        }
        r /= d;
      }
      return sqrt(r);
    }
    }
  }
  void Transform::forward()
  {
    fmodified = false;
    aidesys::alert(fvalues == NULL, "illegal-state", "in backwardtuning::Transform::forward/getF you must set the parameters before use");
    for(unsigned int t = 0, nt = N - 1; t < T; t++, nt += N + N - 1) {
      for(unsigned int n = 0; n < N; n++, nt--) {
        fvalues[nt] = f(n, t);
      }
    }
  }
}
