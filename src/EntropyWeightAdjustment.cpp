#include "EntropyWeightAdjustment.hpp"
#include "std.hpp"
#include "stats.hpp"
#include <float.h>

namespace backwardtuning {
  EntropyWeightAdjustment::EntropyWeightAdjustment(JSON parameters) : Transform(parameters), WeightAdjustment(parameters)
  {}
  EntropyWeightAdjustment::~EntropyWeightAdjustment()
  {
    delete[] histo;
  }
  std::string EntropyWeightAdjustment::asString() const
  {
    return aidesys::echo("EntropyWeightAdjustment: { " + WeightAdjustment::asString() + " }");
  }
  double EntropyWeightAdjustment::c() const
  {
    unsigned int count = No * T;
    // Computes bounds and 2nd order statistics
    min = DBL_MAX;
    double max = -DBL_MAX, m0 = 0, m1 = 0, m2 = 0;
    for(unsigned int t = 0; t < T; t++) {
      for(unsigned int n = 0; n < No; n++) {
        double v = getF(n, t);
        if(v < min) {
          min = v;
        }
        if(v < max) {
          max = v;
        }
        m0++, m1 += v, m2 += v * v;
      }
    }
    double v = m0 < 2 ? 0 : (m2 - m1 * m1 / m0) / (m0 - 1), s = v < DBL_EPSILON ? 0 : sqrt(v);
    // Estimates the histogram size
    unsigned int hsize = s > 0 ? (int) rint((max - min) * pow(count, 1.0 / 3.0) / (3.49 * s)) : count / 20;
    delete[] histo;
    histo = new unsigned int[hsize];
    // Fills the histogram
    double hscale = max > min ? (unsigned int) (hsize / (max - min)) : 0;
    for(unsigned int t = 0; t < T; t++) {
      for(unsigned int n = 0; n < No; n++) {
        histo[(unsigned int) (hscale * (getF(n, t) - min))]++;
      }
    }
    // Calculates the entropy
    double entropy = 0;
    for(unsigned int h = 0; h < hsize; h++) {
      double p = histo[h];
      if(p > 0) {
        entropy -= p * log(p);
      }
    }
    entropy = (entropy / count + log(count / hscale)) / log(2);
    return entropy;
  }
  double EntropyWeightAdjustment::dc(unsigned int m, unsigned int s) const
  {
    unsigned int h = (hscale * (getF(m, s) - min));
    h = h > 0 ? h : 1;
    return (log(histo[h]) - log(histo[h - 1])) / log(2);
  }
}
