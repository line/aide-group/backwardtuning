#include "AdjustableTransform.hpp"
#include <cstring>
#include <float.h>
#include "std.hpp"

namespace backwardtuning {
  AdjustableTransform::AdjustableTransform(JSON parameters) : Transform(parameters), WeightAdjustment(parameters), WeightRegularization(parameters)
  {
    R = parameters.get("R", -1), R = R == 0 ? -1 : R;
    K0 = parameters.get("K0", -1), K0 = K0 < K ? K0 : K;
    bvalues = new double[N * T];
    for(unsigned int nt = 0; nt < N * T; bvalues[nt++] = NAN) {}
    initConnectivity();
  }
  AdjustableTransform::AdjustableTransform(const AdjustableTransform& transform) : Transform(transform), WeightAdjustment(transform.parameters), WeightRegularization(transform.parameters), bmodified(true), R(transform.R), K0(transform.K0), fully_connected(transform.fully_connected), global_weights(transform.global_weights), connections(transform.connections), dependencies(transform.dependencies)
  {
    bvalues = new double[N * T];
    for(unsigned int nt = 0; nt < N * T; bvalues[nt++] = NAN) {}
  }
  AdjustableTransform::~AdjustableTransform()
  {
    delete[] bvalues;
  }
  void AdjustableTransform::initConnectivity()
  {
    {
      fully_connected = true;
      for(unsigned int n = 0; fully_connected && n < N; n++) {
        for(unsigned int r = 0; fully_connected && r < std::min(R, T); r++) {
          for(unsigned int m = 0; fully_connected && m < (r == 0 ? n : N); m++) {
            fully_connected &= isConnected(n, m, r);
          }
        }
      }
      if(!fully_connected) {
        connections.clear();
        connections.resize(N);
        for(unsigned int n = 0; n < N; n++) {
          for(unsigned int r = 0; r < std::min(R, T); r++) {
            for(unsigned int m = 0; m < (r == 0 ? n : N); m++) {
              if(isConnected(n, m, r)) {
                connections[n].push_back(std::pair < unsigned int, unsigned int > (m, r));
              } else {
                fully_connected = false;
              }
            }
          }
        }
      }
    }
    {
      global_weights = true;
      for(unsigned int k = 0; global_weights && k < K; k++) {
        for(unsigned int n = 0; global_weights && n < N; n++) {
          global_weights &= isDependent(k, n);
        }
      }
      if(!global_weights) {
        dependencies.clear();
        dependencies.resize(K);
        for(unsigned int k = 0; k < K; k++) {
          for(unsigned int n = 0; n < N; n++) {
            if(isDependent(k, n)) {
              dependencies[k].push_back(n);
            }
          }
        }
      }
    }
  }
  std::string AdjustableTransform::asString() const
  {
    return aidesys::echo("AdjustableTransform: { " + Transform::asString() + " R: %d K0: %d }", R, K0);
  }
  bool AdjustableTransform::isConnected(unsigned int n, unsigned int m, unsigned int d) const
  {
    return true;
  }
  bool AdjustableTransform::isDependent(unsigned int n, unsigned int k) const
  {
    return true;
  }
  double AdjustableTransform::df(unsigned int m, unsigned int s, unsigned int n, unsigned int t) const
  {
    aidesys::alert(true, "illegal-state", "in backwardtuning::AdjustableTransform::df(m,s,n,t), this virtual method must be overridden");
    return NAN;
  }
  double AdjustableTransform::df(unsigned int k, unsigned int n, unsigned int t) const
  {
    aidesys::alert(true, "illegal-state", "in backwardtuning::AdjustableTransform::df(k,n,t), this virtual method must be overridden");
    return NAN;
  }
  void AdjustableTransform::backward()
  {
    bmodified = false;
    aidesys::alert(bvalues == NULL, "illegal-state", "in backwardtuning::Transform::backward/getB you must set the parameters before use");
    if(fmodified) {
      forward();
    }
    for(int t = T - 1; t >= 0; t--) {
      for(unsigned int n = 0; n < N; n++) {
        unsigned nt = n + t * N;
        double v = -dc(n, t);
        if(fully_connected) {
          for(unsigned int m = 0; m < N; m++) {
            for(unsigned int s = m <= n ? t + 1 : t; s <= std::min(t + R, T - 1); s++) {
              unsigned int ms = m + s * N;
              v += bvalues[ms] * df(m, s, n, t);
            }
          }
        } else {
          for(auto it = connections.at(n).crbegin(); it != connections.at(n).crend(); it++) {
            if((unsigned int) t >= it->second) {
              unsigned int m = it->first, s = t - it->second, ms = m + s * N;
              aidesys::alert(!((unsigned int) (m <= N ? t + 1 : t) <= s && s <= std::min((unsigned int) t + R, T - 1) && ((unsigned int) t < s || n < m)), "illegal-state", "in backwardtuning::AdjustableTransform::backward spurious index (n: %d, m: %d) < N: %d or (t: %d, s: %d) < T: %d", n, m, N, t, s, T);
              v += bvalues[ms] * df(m, s, n, t);
            }
          }
        }
        aidesys::alert(std::isnan(v), "illegal-state", "in backwardtuning::AdjustableTransform::backward spurious NaN value at n: %d and t: %d", n, t);
        bvalues[nt] = v;
      }
    }
  }
  double AdjustableTransform::adjust(unsigned int count, double epsilon)
  {
    // Minimal weight adjustment gain
    epsilon = std::max(epsilon, sqrt(DBL_EPSILON));
    // Predefined weight adjustment gain increase and decrease
    static const double upsilon_less = 0.5, upsilon_more = (sqrt(5) + 1) / 2; // choosing the golden ratio just for fun :)
    double W[K];
    if(bmodified) {
      backward();
    }
    if(costs.size() == 0) {
      costs.push_back(c());
    }
    for(unsigned int t = 0; t < count && epsilon < upsilon; t++) {
      memcpy(W, weights, sizeof(double) * K);
      upsilons.push_back(upsilon);
      adjustWeights();
      forward(), backward(), costs.push_back(c());
      aidesys::alert(costs.size() < 2, "illegal-state", "in backwardtuning::AdjustableTransform::adjust spurious costs buffer size %d", (unsigned int) costs.size());
      if(costs.at(costs.size() - 2) <= costs.back()) {
        setWeights(W), forward(), backward(), costs.pop_back();
        upsilon *= upsilon_less;
      } else {
        upsilon *= upsilon_more;
      }
    }
    return costs.back();
  }
  void AdjustableTransform::adjustWeights()
  {
    for(unsigned k = 0; k < K0; k++) {
      double v = 0;
      for(unsigned int t = 0, nt = 0; t < T; t++) {
        if(global_weights) {
          for(unsigned int n = 0; n < N; n++, nt++) {
            v += bvalues[nt] * df(k, n, t);
          }
        } else {
          for(auto it = dependencies.at(k).cbegin(); it != dependencies.at(k).cend(); it++) {
            unsigned int n = *it;
            v += bvalues[nt] * df(k, n, t);
          }
        }
      }
      aidesys::alert(std::isnan(v), "illegal-state", "in backwardtuning::AdjustableTransform::adjustWeight spurious NaN correction value at k: %d", k);
      aidesys::alert(std::isnan(l(k)), "illegal-state", "in backwardtuning::AdjustableTransform::adjustWeight spurious NaN correction l factor at k: %d", k);
      weights[k] += upsilon * l(k) * v;
    }
  }
}
