#include "QuadLinWeightRegularization.hpp"
#include "std.hpp"

namespace backwardtuning {
  QuadLinWeightRegularization::QuadLinWeightRegularization(JSON parameters) : Transform(parameters), WeightRegularization(parameters)
  {
    q.setWo(W_o = parameters.get("W_o", 1.0));
  }
  std::string QuadLinWeightRegularization::asString() const
  {
    return aidesys::echo("QuadLinWeightRegularization: { " + WeightRegularization::asString() + " W_o: %g }", W_o);
  }
}
