#include "BufferedOutputWeightAdjustment.hpp"
#include <math.h>
#include "std.hpp"

namespace backwardtuning {
  BufferedOutputWeightAdjustment::BufferedOutputWeightAdjustment(JSON parameters) : Transform(parameters), WeightAdjustment(parameters), OutputWeightAdjustment(parameters), input(parameters.clone().set("Ni", parameters.get("No", 0)))
  {}
  std::string BufferedOutputWeightAdjustment::asString() const
  {
    return "BufferedOutputWeightAdjustment: { " + OutputWeightAdjustment::asString() + " }";
  }
}
