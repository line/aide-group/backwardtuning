#ifndef __backwardtuning_LpOutputWeightAdjustment__
#define __backwardtuning_LpOutputWeightAdjustment__

#include "OutputWeightAdjustment.hpp"

namespace backwardtuning {
  /**
   * @class LpOutputWeightAdjustment
   * @description Defines a input/output dynamical system tuned by backward learning.
   * - It implements the [c(·)](./OutputWeightAdjustment.html#c) and [dc(·)](./OutputWeightAdjustment.html#dc) method of the cost function.
   *    - `c = sum_{nt} |o_n(t) - x_n(t)|^p / p` with `1 < p <= 2`, i.e., `L^p`, with `0 <= c <= N T M^p / p`, `|x_n(t)| <= M`.
   * @extends OutputWeightAdjustment
   * @param {JSON} parameters Object parameter, including parent class parameters:
   * - `d [= 2.0]` The criterion exponent, we must have `1 < d <= 2`.
   */
  class LpOutputWeightAdjustment: virtual public OutputWeightAdjustment {
    double p = 2, p1 = 1;
public:
    LpOutputWeightAdjustment(const LpOutputWeightAdjustment& transform) = delete;
    void operator = (const LpOutputWeightAdjustment&) = delete;
    LpOutputWeightAdjustment(JSON parameters);
    virtual ~LpOutputWeightAdjustment() {}
    virtual std::string asString() const;

    virtual double c() const;
    virtual double dc(unsigned int m, unsigned int s) const
    {
      double e = getF(m, s) - o(m, s);
      return s < T && m < No ? p1 == 1 ? e : pow(e, p1) : 0;
    }
  };
}
#endif
