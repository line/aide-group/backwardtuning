#include "ReservoirTransform.hpp"
#include "file.hpp"
#include <stdlib.h>
#include <set>

namespace backwardtuning {
  void ReservoirTransform::init(wjson::Value& parameters)
  {
    if(parameters.isMember("weights") && buffer.getUnitCount("weights") == 0) {
      aidesys::alert(!aidesys::exists(parameters.get("weights", "")), "illegal-argument", "in backwardtuning::ReservoirTransform::init the weights file '" + parameters.get("weights", "") + "' does not exists");
      // Loads the weights and adjust the parameters accordingly
      buffer.load(parameters.get("weights", ""));
      parameters.set("N_Win", buffer.getUnitCount("Win"));
      parameters.set("Ni_Win", buffer.getTimeCount("Win"));
      parameters.set("Ni", parameters.get("Ni", parameters.get("Ni_Win", 0)));
      parameters.set("No_Wout", buffer.getUnitCount("Wout"));
      parameters.set("N_Wout", buffer.getTimeCount("Wout"));
      parameters.set("No", parameters.get("No", parameters.get("No_Wout", 0)));
      parameters.set("N_W_row", buffer.getUnitCount("W"));
      parameters.set("N_W_col", buffer.getTimeCount("W"));
      parameters.set("N", parameters.get("N", parameters.get("No", 0) + parameters.get("N_Wout", 0)));
      // Initializes the connections
      {
        adjust = parameters.get("adjust", 0x7);
        unsigned int K0 = 0, K = 0;
        std::string channels[] = { "Wout", "W", "Win" };
        // First select adjustable weights and then select fixed weights
        for(unsigned int b = 0; b < 2; b++) {
          for(unsigned int c = 0; c < 3; c++) {
            if((adjust & (0x1 >> c)) == (unsigned int) (1 - b)) {
              // Scans all matrix weights
              for(unsigned int j = 0; j < buffer.getUnitCount(channels[c]); j++) {
                for(unsigned int i = 0; i < buffer.getTimeCount(channels[c]); i++) {
                  double w = buffer.get(channels[c], j, i);
                  // Only considers non zero weights
                  if(w != 0.0) {
                    unsigned int n, m;
                    // Creates the related connections
                    switch(c) {
                    case 0: // Wout
                      n = j, m = No + i;
                      break;
                    case 1: // W
                      n = No + j, m = No + i;
                      break;
                    case 2: // Win
                      n = No + j, m = N + i;
                      break;
                    }
                    bool ok = createConnection(n, m, K);
                    aidesys::alert(!ok, "illegal-state", "in backwardtuning::ReservoirTransform::init trying to set a connection twice K: %d at %s#%d[%d,%d] for add(%d, %d)", K, channels[c].c_str(), c, j, i, n, m);
                    // Sets the weight value
                    buffer.add("weights", K, 0, w);
                    // Counts the weights
                    if(b == 0) {
                      K0++;
                    }
                    K++;
                  }
                }
              }
            }
          }
        }
        for(unsigned int c = 0; c < 3; c++) {
          buffer.clear(channels[c]);
        }
        parameters.set("K", K);
        parameters.set("K0", K0);
      }
    }
    // Loads data if appropriate
    if(parameters.isMember("dataset") && buffer.getUnitCount("input") == 0 && buffer.getUnitCount("output")) {
      aidesys::alert(!aidesys::exists(parameters.get("dataset", "")), "illegal-argument", "in backwardtuning::ReservoirTransform::ReservoirTransform the dataset file '" + parameters.get("dataset", "") + "' does not exists");
      buffer.load(parameters.get("dataset", ""));
      // Sets the input buffer if defined
      if(parameters.isMember("input")) {
        aidesys::alert(!buffer.getChannels().isMember(parameters.get("input", "")), "illegal-argument", "in backwardtuning::ReservoirTransform::ReservoirTransform in the dataset file '" + parameters.get("dataset", "") + "' the channel ''" + parameters.get("input", "") + "'' does not exists in '" + buffer.asString() + "'");
        parameters.set("Ni_input", buffer.getUnitCount(parameters.get("input", "")));
        parameters.set("T_input", buffer.getTimeCount(parameters.get("input", "")));
      }
      // Sets the output buffer if defined
      if(parameters.isMember("output")) {
        aidesys::alert(!buffer.getChannels().isMember(parameters.get("output", "")), "illegal-argument", "in backwardtuning::ReservoirTransform::ReservoirTransform in the dataset file '" + parameters.get("dataset", "") + "' the channel ''" + parameters.get("output", "") + "''does not exists in '" + buffer.asString() + "'");
        parameters.set("No_output", buffer.getUnitCount(parameters.get("output", "")));
        parameters.set("T_output", buffer.getTimeCount(parameters.get("output", "")));
      }
      // Sets the T value
      {
        parameters.set("T", parameters.get("T", parameters.get("T_output", parameters.get("T_input", 0))));
      }
    }
    // Checks the parameter values coherence
    {
      parameters.set("R", 1);
      aidesys::alert(parameters.get("N_Win", 0) != parameters.get("N_Wout", 0) ||
                     parameters.get("Ni_Win", 0) != parameters.get("Ni", 0) ||
                     parameters.get("No_Wout", 0) != parameters.get("No", 0) ||
                     parameters.get("N_Wout", 0) != parameters.get("N_W_row", 0) ||
                     parameters.get("N_W_row", 0) != parameters.get("N_W_col", 0) ||
                     (parameters.isMember("Ni_input") && parameters.get("Ni_input", 0) != parameters.get("Ni", 0)) ||
                     (parameters.isMember("T_input") && parameters.get("T_input", 0) != parameters.get("T", 0)) ||
                     (parameters.isMember("No_output") && parameters.get("No_output", 0) != parameters.get("No", 0)) ||
                     (parameters.isMember("T_output") && parameters.get("T_output", 0) != parameters.get("T", 0)) ||
                     parameters.get("N", 0) != parameters.get("No", 0) + parameters.get("N_Wout", 0) ||
                     parameters.get("N", 0) == 0 ||
                     parameters.get("T", 0) == 0 ||
                     parameters.get("K", 0) == 0 ||
                     parameters.get("R", 0) == 0,
                     "illegal-argument", "in backwardtuning::ReservoirTransform::ReservoirTransform the weights file '" + parameters.get("weights", "") + "' and the dataset file '" + parameters.get("dataset", "") + "' the parameters '" + parameters.asString() + "' are incoherent you must have: N == (N_wout == N_Win == N_W_row == N_W_row) + (No == No_Wout == No_output > 0) > 0 && Ni = Ni_Win == Ni_input > 0 && T == T_input == T_output > 0 && K > 0");
    }
  }
  ReservoirTransform::ReservoirTransform(JSON parameters) : Transform(parameters), WeightAdjustment(parameters), WeightRegularization(parameters), AdjustableTransform(parameters), TransformInput(parameters), OutputWeightAdjustment(parameters), NLNTransform(parameters), BufferedOutputWeightAdjustment(parameters)
  {
    // Reports weights
    {
      for(unsigned k = 0; k < K; k++) {
        setW(k, buffer.get("weights", k, 0));
      }
      buffer.clear("weights");
    }
    // Sets input/output data
    {
      if(parameters.isMember("input")) {
        TransformInput::setValues(buffer.getSamples(parameters.get("input", "")), buffer.getUnitCount(parameters.get("input", "")), buffer.getTimeCount(parameters.get("input", "")));
        buffer.clear("input");
      }
      if(parameters.isMember("output")) {
        BufferedOutputWeightAdjustment::setValues(buffer.getSamples(parameters.get("output", "")), buffer.getUnitCount(parameters.get("output", "")), buffer.getTimeCount(parameters.get("output", "")));
        buffer.clear("output");
      }
    }
  }
  std::string ReservoirTransform::asString() const
  {
    return "ReservoirTransform: { " + AdjustableTransform::asString() + " adjust: [ " + (adjust && 0x1 ? "Wout " : "") + (adjust && 0x2 ? "W " : "") + (adjust && 0x4 ? "Win " : "") + +"] }";
  }
}
