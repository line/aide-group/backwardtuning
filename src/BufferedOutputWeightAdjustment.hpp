#ifndef __backwardtuning_BufferedOutputWeightAdjustment__
#define __backwardtuning_BufferedOutputWeightAdjustment__

#include "OutputWeightAdjustment.hpp"
#include "Value.hpp"
#include "TransformInput.hpp"

namespace backwardtuning {
  /**
   * @class BufferedOutputWeightAdjustment
   * @description Defines the output adjustment interface.
   * - The `o(·)` desired output function is implemented.
   * @extends OutputWeightAdjustment
   * @param {JSON} parameters Object parameter, including parent class parameters.
   */
  class BufferedOutputWeightAdjustment: virtual public OutputWeightAdjustment {
    TransformInput input;
public:
    BufferedOutputWeightAdjustment(const BufferedOutputWeightAdjustment& transform) = delete;
    void operator = (const BufferedOutputWeightAdjustment&) = delete;
    BufferedOutputWeightAdjustment(JSON parameters);
    virtual ~BufferedOutputWeightAdjustment() {}
    virtual std::string asString() const;

    /**
     * @function setValues
     * @memberof BufferedOutputWeightAdjustment
     * @instance
     * @description Sets the buffer values.
     * @param {Array|Function} values A 1D `double[Ni * T]` array with `i_n(t) = values[n + Ni * t]`, values are copied.
     * - It can also be provided as a `std::vector<double>` container.
     * - It can also be provided as a function or as a lambda: <br/>`transform.setValues([] (unsigned int n, unsigned int t) { return input(n, t); }`
     * @param {uint} No Number of desired units.
     * @param {uint} T Number of time steps.
     */
    void setValues(const double *values, unsigned int No, unsigned int T)
    {
      input.setValues(values, No, T);
    }
    void setValues(const std::vector < double > &values, unsigned int No, unsigned int T)
    {
      input.setValues(values, No, T);
    }
    void setValues(std::function < double (unsigned int, unsigned int) > values, unsigned int No, unsigned int T)
    {
      input.setValues(values, No, T);
    }
    /**
     * @function o
     * @memberof BufferedOutputWeightAdjustment
     * @instance
     * @description Returns the desired output value.
     * @param {uint} n Input unit index.
     * @param {uint} t Current time.
     * @return {double} The corresponding value, or `NAN` if out of bound.
     */
    virtual double o(unsigned int n, unsigned int t) const
    {
      return input.i(n, t);
    }
  };
}
#endif
