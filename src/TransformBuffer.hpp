#ifndef __backwardtuning_TransformBuffer__
#define __backwardtuning_TransformBuffer__

#include <map>
#include <vector>
#include <set>
#include "Value.hpp"

namespace backwardtuning {
  /**
   * @class TransformBuffer
   * @description Defines a data buffer with some statistics estimation.
   * - The data is structured in terms of named ``channel´´ storing spatio-temporal values indexed bay `(n, t)`.
   */
  class TransformBuffer {
    // Used as a default value
    std::vector < double > empty;
    // The channel names
    std::set < std::string > channels;
    // A map(channel-name, unit-index) -> vector(time-index)
    std::map < std::pair < std::string, unsigned int >, std::vector < double >> buffers_t;
    // => A map(channel-name, time-index) -> vector(unit-index)
    std::map < std::pair < std::string, unsigned int >, std::vector < double >> buffers_n;
protected:
    // => The N upper bound
    std::map < std::string, unsigned int > Nsup;
    // => The T upper bound
    std::map < std::string, unsigned int > Tsup;
public:

    /**
     * @function add
     * @memberof TransformBuffer
     * @instance
     * @description Adds a value in a buffer.
     * @param {string} c The buffer channel name.
     * @param {uint} n Input unit index.
     * @param {uint} t Current time.
     * @param {double} value The current value;
     * @return {double} The input value to allow constructs of, e.g., the form:<br>
     * `double v = system.add("input", n, t, system.i(n, t));`
     */
    double add(String c, unsigned int n, unsigned int t, double value);

    /**
     * @function load
     * @memberof TransformBuffer
     * @instance
     * @description Adds values from a JSON file.
     * @param {string} filename The file name.
     *  - Multi-unit values are given in an unit-indexed array of array with time-slice values.
     *  - Mono-unit values are given in an array with time-slice values.
     *  - Channel values are given in a record, e.g.: <br/>`{ channel-name: [[x_00 x_01 ...] ...]}`.
     *  - The default channel name is the file basename.
     */
    void load(String filename);
private:
    void load(String name, JSON data);
public:

    /**
     * @function clear
     * @memberof TransformBuffer
     * @instance
     * @description Clears a buffer channel values.
     * - Without a channel name, clear all values.
     * @param {string} [c] The buffer channel name.
     */
    void clear(String c = "");

    /**
     * @function get
     * @memberof TransformBuffer
     * @instance
     * @description Returns a buffered value.
     * @param {string} c The buffer channel name.
     * @param {uint} n The unit index.
     * @param {uint} t Current time.
     * @return {double} The stored value, or `NAN` if undefined.
     */
    double get(String c, unsigned int n, unsigned int t) const;

    /**
     * @function getUnitCount
     * @memberof TransformBuffer
     * @instance
     * @description Returns the number of units.
     * @param {string} c The buffer channel name.
     * @return {uint} The  number of units.
     */
    unsigned int getUnitCount(String c) const
    {
      return Nsup.find(c) == Nsup.end() ? 0 : Nsup.at(c);
    }
    /**
     * @function getTimeCount
     * @memberof TransformBuffer
     * @instance
     * @description Returns the number of time-steps.
     * @param {string} c The buffer channel name.
     * @return {uint} The  number of time-steps.
     */
    unsigned int getTimeCount(String c) const
    {
      return Tsup.find(c) == Tsup.end() ? 0 : Tsup.at(c);
    }
    /**
     * @function getUnitSlice
     * @memberof TransformBuffer
     * @instance
     * @description Returns a the temporal value of a given buffer.
     * - Statistics on the stored values can be obtained using: [`aidesys::getStat(·)`](https://line.gitlabpages.inria.fr/aide-group/aidesys/stats.html#.getStat).
     * @param {string} c The buffer channel name.
     * @param {uint} n Input unit index.
     * @return {Array} A `std::vector<double>` with the stored values, indexed by `t`.
     */
    const std::vector < double >& getUnitSlice(String c, unsigned int n) const;

    /**
     * @function getTimeSlice
     * @memberof TransformBuffer
     * @instance
     * @description Returns a the temporal value of a given buffer.
     * - Statistics on the stored values can be obtained using: [`aidesys::getStat(·)`](https://line.gitlabpages.inria.fr/aide-group/aidesys/stats.html#.getStat).
     * - Time slices are buffered under the `$c-@t:$t` channel name, where `$c` stands for the channel name and `$t` the time value.
     * @param {string} c The buffer channel name.
     * @param {uint} t Current time.
     * @return {Array} A `std::vector<double>` with the stored values, indexed by `n`.
     */
    const std::vector < double >& getTimeSlice(String c, unsigned int t) const;

    /**
     * @function getSamples
     * @memberof TransformBuffer
     * @instance
     * @description Returns all values of a given channel.
     * - Statistics on the stored values can be obtained using: [`aidesys::getStat(·)`](https://line.gitlabpages.inria.fr/aide-group/aidesys/stats.html#.getStat).
     * @param {string} c The buffer channel name.
     * @return {Array} A `std::vector<double>` with the stored values, indexed row wise: `value(n, t) = buffer[n + N * t]`.
     */
    const std::vector < double >& getSamples(String c) const;

    /**
     * @function getChannels
     * @memberof TransformBuffer
     * @instance
     * @description Returns the set of all channels.
     * - It can be used to check if a channel exists: `buffer.getChannels().isMember("channel")`.
     * - It returns a data structure of the form:
     * ```
     * {
     *   channel: { N: ... T: ... }
     *   ../..
     * }
     * ```
     * @return {JSON} The given wjson data structure.
     */
    JSON getChannels() const;

    /**
     * @function asString
     * @memberof TransformBuffer
     * @instance
     * @description Returns the object parameters as a parsable weak json string.
     * @return {string} All channels unit and time counts.
     */
    std::string asString() const;
  };
}
#endif
