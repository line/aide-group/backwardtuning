#ifndef __backwardtuning_Sigmoid__
#define __backwardtuning_Sigmoid__

#include <math.h>

namespace backwardtuning {
  /**
   * @class Sigmoid
   * @description Defines a sigmoid profile.
   * - The usual `s(w) = 1 / (1 + exp(-4 w))` profile is implemented in a tabulation table.
   * @param {double} [epsilon=1e-4] The precision parameter for tabulated values in `[1e-6, 1/2[`.
   */
  class Sigmoid {
    unsigned int N = 0;
    double *values = NULL, *dvalues = NULL, a = 0, b = 0;
public:
    Sigmoid(double epsilon = 1e-4);
    ~Sigmoid();

    /**
     * @function f
     * @memberof Sigmoid
     * @instance
     * @description Returns the profile value.
     * @param {double} w The profile argument.
     * @return {double} The profile value.
     */
    double f(double w) const
    {
      int n = rint(a * w + b);
      return values[n < 0 ? 0 : (int) N <= n ? N - 1 : n];
    }
    /**
     * @function df
     * @memberof Sigmoid
     * @instance
     * @description Returns the profile derivative value.
     * @param {double} w The profile argument.
     * @return {double} The profile derivative value.
     */
    double df(double w) const
    {
      int n = rint(a * w + b);
      return dvalues[n < 0 ? 0 : (int) N <= n ? N - 1 : n];
    }
  };
}
#endif
