#ifndef __backwardtuning_EntropyWeightAdjustment__
#define __backwardtuning_EntropyWeightAdjustment__

#include "OutputWeightAdjustment.hpp"

namespace backwardtuning {
  /**
   * @class EntropyWeightAdjustment
   * @description Defines a input/output dynamical system tuned by backward learning.
   * - It implements the [c(·)](./OutputWeightAdjustment.html#c) and [dc(·)](./OutputWeightAdjustment.html#dc) method of the cost function.
   *    - `c = sum_{k} p_k log_2(p_k)` where `p_k` is computed from the `x_(n, t), 0 <= n < No, 0 <= t < T` histogram.
   * @extends WeightAdjustment
   * @param {JSON} parameters Object parameter, including parent class parameters.
   */
  class EntropyWeightAdjustment: virtual public WeightAdjustment {
    mutable double hscale = 0, min = 0;
    mutable unsigned int *histo = NULL;
public:
    EntropyWeightAdjustment(const EntropyWeightAdjustment& transform) = delete;
    void operator = (const EntropyWeightAdjustment&) = delete;
    EntropyWeightAdjustment(JSON parameters);
    virtual ~EntropyWeightAdjustment();
    virtual std::string asString() const;

    virtual double c() const;
    virtual double dc(unsigned int m, unsigned int s) const;
  };
}
#endif
