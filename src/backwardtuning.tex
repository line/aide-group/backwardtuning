\slide{Backward-tuning: motivation}{
  We consider weight estimation in recurrent input-output distributed computations, called network, reusing a “good old” control-theory principle, made explicit here using the notion of backward-tuning, as developed in \href{./RR-9100.pdf}{\cite{vieville_recurrent_2017}}.

  \stwo{0.4}{0.5}{\centerline{\includegraphics[width=0.4\textwidth]{./recurrent-architecture.png}}}{~\\
    The basic idea is to consider a parameterized distributed computation and adjust the parameters ${\bf W}$, called weights, in order to optimize a cost function $c(\cdot)$ evaluated on the state values ${\bf x}$:
    \eqline{\min_{\bf W} c({\bf x})}
}}

\slide{Backward-tuning: algorithm}{
  Reformulating the problem yields a numerically stable and efficient distributed estimation, without any meta-parameter (derivations are in the appendix).
  
  \stwo{0.4}{0.5}{\centerline{\includegraphics[width=0.4\textwidth]{./algorithm-structure.png}}}{~
    \\ 1/ The {\em forward} simulation computes the internal and output state values, given the weights and the input.
    \\ 2/ The {\em backward} tuning computes, backward in time, the 1st order influence of state values variations, on the nest step cost.
    \\ 3/ These values are combined to adjust the weights, locally to a unit, up to the 1st order.
}} 

\slide{Backward-tuning: interpretation}{
  \sitem{Tuning simply combines cost variation, scaled by forward state variation:}
  ~\\\sfbox{\eqline{\begin{array}{l}\mbox{tuning-value} = -\mbox{cost-state-variation} \\ \;\;\;\; + \sum_{\mbox{efferent-units}} \mbox{next-tuning-value} \cdot \mbox{forward-state-variation} \end{array}}}
  ~\\
  \sitem{It predicts the future influence of internal unit's values}
  \sitem{It leads to a pseudo Hebbian post-unit / pre-unit rule}
  \ssitem{Post-unit tuning value / Pre-unit weight sensitivity}
  \ssitem{Yielding a falsifable hypothesis in terms of biological plausibility}
  \sitem{It is a form of predictive coding of the backward error}
  \ssitem{Backward tuning vanishes at a [local] optimum}
}

\slide{Backward-tuning: interest}{
  \sitem{Avoid direct gradient backpropagation numerical problems}
  \ssitem{E.g., vanishing or divergent magnitude curse}
  \ssitem{I.e., decouple backward propagation with weight adjustment}
  ~\\
  \sitem{Weight adjustment is really local to a unit (while not local in time)}
  \ssitem{Thus no complex gradient computation}
  \ssitem{The weight adjustment gain is easily auto-tuned}
  ~\\
  \sitem{Weight adjustment has a local closed form solution …}
  \ssitem{ … for a given [quadratic] regularization scale}
  \ssitem{The regularization scale is auto-tuned}
}

\slide{Backward-tuning: limitations}{
  \sitem{Requires a backward in time process}
  \ssitem{I.e., a kind of back propagation in time of tuning value (no gradient)}
  ~\\
  \sitem{Basically only an iterative off-line process in a fixed time window}
  \ssitem{Through generalization to online adjustement is possible}
  ~\\
  \sitem{Curiously … this very standard method is unused …}
  \ssitem{ … is there some hidden caveat?}
}

\slide{Backward-tuning: implementation}{
 A modular, C++ and Python wrapped, object oriented class set.
 \centerline{\includegraphics[width=0.9\textwidth]{./class-hierarchy.png}}
}

\slide{Backward-tuning: tiny experimentation}{~
  Adjustment of a small network on random input and output:
  \\~~~~~~~~~~~~~~ N = 10, K = 30, T = 20 \\
  \stwo{0.4}{0.5}{\centerline{\includegraphics[width=0.4\textwidth]{./sigmoidtransformdemo/master-costs.png}}}{~
  \\ - Important convergence at 1st step
  \\ - Fast auto-tuned convergence
  \\ - Backward tuning values are stable
 }
 Also checked on master/student paradigm. \\
 Also check with various network parameters.
}
  
\sappendix

\slide{Backward-tuning: derivation 1/4}{
We consider a $N$-dimensional vectorial causal discrete-time dynamical system observed a $T$ samples, defined by a recurrent equation:
\eqline{x_n(t) = {}_{\bf w}f_n(t, \cdots x_m(s) \cdots), 0 \le s < t < T, 0 \le n < N, 0 \le m < N,}
parameterized by K static parameters ${\bf w} \in {\cal R}^K$.
\\~
\\ - This includes $N_i$ ``input'' that are simply $x_n(t) = f_n(t)$ without parameter and state dependencies.
\\ - This includes $N_o$ ``output'' that are simply $x_n(t)$ that are observable, by convention for $0 \le n < N_o \le N$.
}
\slide{Backward-tuning: derivation 2/4}{
The goal is to adjust these parameters minimizing:
\eqline{\begin{array}{l}\min_{\bf w x z} {\cal L}({\bf w x z}), {\cal L}(\cdot) \deq \\
  \underbrace{c({\bf x})}_{\mbox{cost function}} +
  \underbrace{\sum_{nt} z_n(t) \, (x_n(t) - {}_{\bf w}f_n(t, \cdots))}_{\mbox{system dynamic}} +
  \underbrace{\|{\bf w} - \hat{\bf w}\|_{{\bf \Lambda}(\hat{\bf w}, \hat{\bf x}, \hat{\bf z})^{-1}}^2}_{\mbox{parameter regularization}} \end{array}}
where $z_n(t)$ are Lagrange multipliers, used to derive while $\hat{\bf w}, \hat{\bf x}, \hat{\bf z}$ stands for initial or previous estimates of these values. Here ${\bf \Lambda}(\cdot)$ is restrained to a diagonal matrix of diagonal element $\upsilon \, l_k$, where $\upsilon$ is the usual global weight adjustment gain, here \href{https://line.gitlabpages.inria.fr/aide-group/backwardtuning/weightadjustment.pdf}{\underline{auto-tuned}}.
}
\slide{Backward-tuning: derivation 3/4}{
This yields the following equations, from the normal equations: \\
\begin{itemize}
  \item {\em forward simulation}: $x_n(t) \leftarrow {}_{\bf w} f_n(t, \cdots)$, \\$n \in \{0, N\{, t \in \{0, T\{$ (from $\partial_{z_n(t)} {\cal L} = 0$); \\
  \item {\em backward tuning}: $z_n(t) \leftarrow - \partial_{x_n(t)}c(\cdot)  + \sum_{ms} z_m(s) \, \partial_{x_n(t)} {}_{\bf w}f_m(s, \cdots)$, \\$n \in \{0, N\{, t \in \}T, 0\}$ (from $\partial_{x_n(t)} {\cal L} = 0$); \\
  \item {\em weight adjustment}: $w_k \leftarrow \hat{w}_k + \upsilon \, l_k \, \sum_{nt} z_n(t) \, \partial_{w_k}{}_{\bf w}f_n(t, \cdots)$ \\ (from $\partial_{\bf w} {\cal L} = 0$).
\end{itemize}
}
\slide{Backward-tuning: derivation 4/4}{
This has been developed in details at a rather general level in \cite{vieville_recurrent_2017}. This is a simplified application of the Pontryagin's minimum principle, well-known in control theory \cite{astrom_theory_1983}, generalizing an idea drafted by \cite{le_cunn_yann_theoretical_1988}.\\

\stwo{0.4}{0.5}{\centerline{\includegraphics[width=0.4\textwidth,height=8cm]{./quadlin.jpg}}}{If $l_k = \frac{(\hat{w}_{k,\bullet} + |\hat{w}_k|)^2}{(2 \, \hat{w}_{k,\bullet} + |\hat{w}_k|)}$, this corresponds to \href{https://line.gitlabpages.inria.fr/aide-group/backwardtuning/quadlin.pdf}{\underline{quadlin}} regularization profile.}

}

