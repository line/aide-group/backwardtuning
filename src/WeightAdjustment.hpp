#ifndef __backwardtuning_WeightAdjustment__
#define __backwardtuning_WeightAdjustment__

#include "Value.hpp"
#include "Transform.hpp"

namespace backwardtuning {
  /**
   * @class WeightAdjustment
   * @description Defines the weight adjustment interface.
   * - The local output cost `c(·)` and cost derivative `dc(·)`, related to minimizing <br>
   * `c(x) + …` <br>
   * as made explicit in the [equation summary](./derivation.pdf)`, has to be implemented.
   * @extends Transform
   * @param {JSON} parameters Object parameter:
   * - `No [= -1]` Number of observable output state units. The `-1` value means that all units are observable.
   * - By convention they are the `No` 1st units, while the remainding `N - No` units are considered as hidden usnits.
   */
  class WeightAdjustment: virtual public Transform {
protected:
    unsigned int No;
public:
    WeightAdjustment(const WeightAdjustment& transform) = delete;
    void operator = (const WeightAdjustment&) = delete;
    WeightAdjustment(JSON parameters);
    virtual ~WeightAdjustment() {}
    virtual std::string asString() const;

    /**
     * @function c
     * @memberof WeightAdjustment
     * @instance
     * @abstract
     * @description Implements the cost computation.
     * @return {double} The corresponding value.
     */
    virtual double c() const;

    /**
     * @function dc
     * @memberof WeightAdjustment
     * @instance
     * @abstract
     * @description Implements the cost state derivative.
     * @param {uint} m Unit index derivative.
     * @param {uint} s Current time derivative.
     * @return {double} The corresponding value.
     */
    virtual double dc(unsigned int m, unsigned int s) const;
  };
}
#endif
