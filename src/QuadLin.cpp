#include "QuadLin.hpp"
#include <math.h>
#include "std.hpp"

namespace backwardtuning {
  void QuadLin::setWo(double w_o)
  {
    aidesys::alert(w_o <= 0, "illegal-argument", "in backwardtuning::QuadLin::setWo w_o = %e is not strictly positive", w_o);
    this->w_o = fabs(w_o);
  }
  double QuadLin::f(double w) const
  {
    return w * w / (w_o + fabs(w));
  }
  double QuadLin::df(double w) const
  {
    double x = w_o + fabs(w);
    return w * (w_o + x) / (x * x);
  }
  double QuadLin::dw(double w) const
  {
    double x = w_o + fabs(w);
    return (x * x) / (w_o + x);
  }
}
