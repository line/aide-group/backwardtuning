#ifndef __backwardtuning_RandomOutputWeightAdjustment__
#define __backwardtuning_RandomOutputWeightAdjustment__

#include "BufferedOutputWeightAdjustment.hpp"
#include "RandomTransformInput.hpp"

namespace backwardtuning {
  /**
   * @class RandomOutputWeightAdjustment
   * @description Defines a random reference signal.
   * - The value is drawn from a normal distribution (of zero mean and unary standard-deviation).
   * - The corresponding `o(·)` [OutputWeightAdjustment](./OutputWeightAdjustment.html) function is implemented.
   * @extends BufferedOutputWeightAdjustment
   * @param {JSON} parameters Object parameter, including parent class parameters.
   *  - `unique [= false]` If true returns the same values for all instances of the class.
   */
  class RandomOutputWeightAdjustment: virtual public BufferedOutputWeightAdjustment {
    bool unique;
    RandomTransformInput transform;
public:
    RandomOutputWeightAdjustment(const RandomOutputWeightAdjustment& transform) = delete;
    void operator = (const RandomOutputWeightAdjustment&) = delete;
    RandomOutputWeightAdjustment(JSON parameters);
    virtual ~RandomOutputWeightAdjustment() {}
    virtual std::string asString() const;

    /**
     * @function o
     * @memberof RandomOutputWeightAdjustment
     * @instance
     * @description Returns the current input value.
     * @param {uint} n Output unit index.
     * @param {uint} t Current time.
     * @return {double} The corresponding value.
     */
    double o(unsigned int n, unsigned int t) const
    {
      return transform.i(n, t);
    }
  };
}
#endif
