#ifndef __backwardtuning_NLNTransform__
#define __backwardtuning_NLNTransform__

#include "Sigmoid.hpp"
#include "AdjustableTransform.hpp"
#include "TransformInput.hpp"
#include <map>

namespace backwardtuning {
  /**
   * @class NLNTransform
   * @description Defines an input/output dynamical transform using vanilla Non-Linear Neural (NLN) network.
   * - So called NL neuron networks are made of unit of the form <br>
   * `x_n(t+1) = leak x_n(t) + (1 - leak) h(>_m W_nm x_m(t))`.
   * - The network parameters are ``W_nm`.
   * - A non-linearity `h(·)` is introduced.
   * - The index map is of this form:
   * ![indexes](./indexes.png)
   * - It implements the [f(·)](./Transform.html#f), and [df(·)](./AdjustableTransform.html#df) functions.
   * @extends AdjustableTransform
   * @extends TransformInput
   * @param {JSON} parameters Object parameter, including parent class parameters:
   * - `leak  [= 0.0]` Leak parameter allowing to stabilize the unit transform.
   * - `connect = true` If true creates `K` random network connections and initializes the weights using Wstdev.
   *   - To obtain a fully connected network, set `K = N * (N + Ni) - N`.
   */

  class NLNTransform: virtual public AdjustableTransform, virtual public TransformInput {
    double leak, uleak;
    static Sigmoid sigmoid;
    unsigned int Nc;
    // Connnectivity array
    // The <efferent-unit-index> -> <weight-index, afferent-unit-index>
    std::vector < std::vector < std::pair < unsigned int, unsigned int >> > unit_connections;
    // The <efferent-unit-index, afferent-unit-index> -> weight-index map, (n, m) -> k
    std::map < std::pair < unsigned int, unsigned int >, unsigned int > network_connection_weights;
    // The <efferent-unit-index, weight-index> -> afferent-unit-index map.
    std::map < std::pair < unsigned int, unsigned int >, unsigned int > network_connection_units;
protected:
    void init(wjson::Value& parameters);

    /**
     * @function createConnection
     * @memberof NLNTransform
     * @instance
     * @protected
     * @description Called by the [init()](Tranform.html#init) routine to register connections.
     * @param {uint} n The efferent unit index.
     * @param {uint} m The afferent unit index.
     * @param {uint} k The connection weight unit index.
     * @return {bool} True if the connection is created, false if the connection was already created.
     */
    bool createConnection(unsigned int n, unsigned int m, unsigned int k);

    /**
     * @function getW
     * @memberof NLNTransform
     * @instance
     * @protected
     * @description Gets the weight value of a connection
     * @param {uint} n The efferent unit index.
     * @param {uint} m The afferent unit index.
     * @return {double} The weight value.
     */
    double getW(unsigned int n, unsigned int m) const
    {
      aidesys::alert(network_connection_weights.find(std::pair < unsigned int, unsigned int > (n, m)) == network_connection_weights.end(), "illegal-state", "in backwardtuning::NLNTransform::getW(n:%d m:%d) undefined connection … ", n, m);
      return AdjustableTransform::getW(network_connection_weights.at(std::pair < unsigned int, unsigned int > (n, m)));
    }
    double getF(unsigned int n, unsigned int k, unsigned int t) const
    {
      aidesys::alert(network_connection_units.find(std::pair < unsigned int, unsigned int > (n, k)) == network_connection_units.end(), "illegal-state", "in backwardtuning::NLNTransform::getF(n:%d k:%d, t: %d) undefined connection … ", n, k, t);
      return AdjustableTransform::getF(network_connection_units.at(std::pair < unsigned int, unsigned int > (n, k)), t);
    }
public:
    NLNTransform(JSON parameters);
    NLNTransform(const NLNTransform& transform);
    virtual ~NLNTransform() {}
    virtual std::string asString() const;

    /**
     * @function setWeights
     * @memberof NLNTransform
     * @instance
     * @description Initializes the current parameter's values from another transform.
     * @param {NLNTransform} source The source transform.
     * - The source must have the same `N`, `T`, `K`, `Ni`, while `R = 1`;
     */
    void setWeights(const NLNTransform& transform);

    /**
     * @function h
     * @memberof NLNTransform
     * @instance
     * @abstract
     * @description Implements the equation non-linearity.
     * - It can be overloaded to adjust the unit not-linearity.
     * - By default implements the normalised sigmoid function.
     * @param {double} x non-linearity input.
     * @return {double} The non-linear value.
     */
    virtual double h(double x) const
    {
      return sigmoid.f(x);
    }
    /**
     * @function dh
     * @memberof NLNTransform
     * @instance
     * @abstract
     * @description Implements the equation non-linearity derivative.
     * - It can be overloaded to adjust the unit not-linearity.
     * - By default implements the normalised sigmoid function derivative.
     * @param {double} x non-linearity input.
     * @return {double} The non-linear value.
     */
    virtual double dh(double x) const
    {
      return sigmoid.df(x);
    }
    virtual bool isConnected(unsigned int n, unsigned int m, unsigned int d) const
    {
      return d == 1 && (n == m || network_connection_weights.find(std::pair < unsigned int, unsigned int > (n, m)) != network_connection_weights.end());
    }
    virtual bool isDependent(unsigned int n, unsigned int k) const
    {
      return network_connection_units.find(std::pair < unsigned int, unsigned int > (n, k)) != network_connection_units.end();
    }
    /**
     * @function g
     * @memberof NLNTransform
     * @instance
     * @description Computes the unit linear combination.
     * @param {uint} n Unit index.
     * @param {uint} s Time to consider.
     * @return The `>_m W_nm x_m(s)` value.
     */
    double g(unsigned int n, unsigned int s) const
    {
      double v = 0;
      if(n < AdjustableTransform::N) {
        for(auto it = unit_connections.at(n).cbegin(); it != unit_connections.at(n).cend(); it++) {
          v += AdjustableTransform::getW(it->first) * AdjustableTransform::getF(it->second, s);
        }
      }
      return v;
    }
    virtual double f(unsigned int n, unsigned int t) const
    {
      return n < N ? leak * Transform::getF(n, t - 1) + uleak * h(g(n, t - 1)) : i(n - N, t);
    }
    virtual double df(unsigned int m, unsigned int s, unsigned int n, unsigned int t) const
    {
      aidesys::alert(s != t - 1 || !isConnected(n, m, 1), "illegal-state", "in backwardtuning::NLNTransform::df(m: %d, s: %s, n: %d, t: %d) spurious indexes", m, s, n, t);
      return n == m ? leak : m < N ? uleak * dh(g(n, t - 1)) * getW(n, m) : 0;
    }
    virtual double df(unsigned int k, unsigned int n, unsigned int t) const
    {
      aidesys::alert(!isDependent(n, k), "illegal-state", "in backwardtuning::NLNTransform::df(k: %d, n: %d, t: %d) spurious indexes", k, n, t);
      return dh(g(n, t - 1)) * getF(n, k, t - 1);
    }
  };
}
#endif
