#include "WeightAdjustment.hpp"
#include <math.h>
#include "std.hpp"

namespace backwardtuning {
  WeightAdjustment::WeightAdjustment(JSON parameters) : Transform(parameters)
  {
    No = parameters.get("No", -1);
    No = No < N ? No : N;
  }
  std::string WeightAdjustment::asString() const
  {
    return aidesys::echo("WeightAdjustment: { No: %d }", No);
  }
  double WeightAdjustment::c() const
  {
    aidesys::alert(true, "illegal-state", "in backwardtuning::WeightAdjustment::c(), this virtual method must be overridden if in use");
    return NAN;
  }
  double WeightAdjustment::dc(unsigned int m, unsigned int s) const
  {
    aidesys::alert(true, "illegal-state", "in backwardtuning::WeightAdjustment::dc(n,t), this virtual method must be overridden");
    return NAN;
  }
}
