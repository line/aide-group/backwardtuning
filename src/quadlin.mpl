assume(w :: real, w_o > 0):

mu := w -> w^2 / (w_o + abs(w)):

series(mu(w), w = 0, 4) assuming w > 0;

series(mu(w), w=infinity, 2);

mu(w_o);

dmu := unapply(simplify(D(mu)(w)), w);

dmu(w_o);

d2mu := unapply(simplify(D(dmu)(w)), w);

d2mu(0);

d2mu(w_o) / d2mu(0);

solve({w^2 / w_o - mu(w) = abs(w) - mu(w)}, w) assuming w > 0; 

plotsetup(jpeg, plotoutput="quadlin.jpg", plotoptions="width=600,height=600");
plot(map(a->subs(w_o=a,mu(w)),[1/4,1/2,1,2,4]),w=-4..4);
plotsetup(x11);

