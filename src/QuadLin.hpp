#ifndef __backwardtuning_QuadLin__
#define __backwardtuning_QuadLin__

#include <math.h>

namespace backwardtuning {
  /**
   * @class QuadLin
   * @description Defines a quadratic-linear profile.
   * @slides quadlin.pdf
   */
  class QuadLin {
    double w_o = 1;
public:

    /**
     * @function setWo
     * @memberof QuadLin
     * @instance
     * @description Sets the current profile parameter value.
     * @param {double} [w_o=1] The profile parameter, must be strictly positive.
     */
    void setWo(double w_o = 1);

    /**
     * @function f
     * @memberof QuadLin
     * @instance
     * @description Returns the profile value.
     * It is defined as: `w^2 / (w_o + |w|)`.
     * @param {double} w The profile argument.
     * @return {double} The profile value.
     */
    double f(double w) const;

    /**
     * @function df
     * @memberof QuadLin
     * @instance
     * @description Returns the profile derivative value.
     * @param {double} w The profile argument.
     * @return {double} The profile derivative value.
     */
    double df(double w) const;

    /**
     * @function dw
     * @memberof QuadLin
     * @instance
     * @description Returns the profile iterative value of a criterion with quadlin regularization.
     * - Given a criterion `L(w) = C(w) + q(w) / u` where `q(·)` is a quadlin profile, from `d_w L(w) = 0` the iterative equation writes: <br>
     * `w = u dw(w) d_w C(w)`, with `d(w) = (w_o + |w|)^2 / (2 w_o + |w|)`.
     * @param {double} w The profile argument.
     * @return {double} The profile derivative value.
     */
    double dw(double w) const;
  };
}
#endif
