#ifndef __backwardtuning_QuadLinWeightRegularization__
#define __backwardtuning_QuadLinWeightRegularization__

#include "Value.hpp"
#include "WeightRegularization.hpp"
#include "QuadLin.hpp"

namespace backwardtuning {
  /**
   * @class QuadLinWeightRegularization
   * @description Defines a [quadlin](./quadlin.pdf) parameter adjustment regularizarion.
   * - It implements the [l(·)](./WeightRegularization.html#dc) update function, as detailled in the [equation summary](./derivation.pdf)`.
   * @extends WeightRegularization
   * @param {JSON} parameters Object parameter, including parent class parameters:
   *  - `W_o [= 1.0]` Parameter regularization criterion, it is a [quadlin](./quadlin.pdf) of parameter `w_o`.
   */
  class QuadLinWeightRegularization: virtual public WeightRegularization {
    QuadLin q;
    double W_o;
public:
    QuadLinWeightRegularization(const QuadLinWeightRegularization& transform) = delete;
    void operator = (const QuadLinWeightRegularization&) = delete;
    QuadLinWeightRegularization(JSON parameters);
    virtual ~QuadLinWeightRegularization() {}
    virtual std::string asString() const;

    virtual double l(unsigned int k) const
    {
      return q.dw(getW(k));
    }
  };
}
#endif
