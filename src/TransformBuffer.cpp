#include "TransformBuffer.hpp"
#include "std.hpp"
#include <math.h>
#include "file.hpp"
#include "regex.hpp"
#include "Value.hpp"

namespace backwardtuning {
  double TransformBuffer::add(String c, unsigned int n, unsigned int t, double value)
  {
    bool new_channel = channels.find(c) == channels.cend();
    if(new_channel) {
      channels.insert(c);
    }
    std::vector < double >& buffers_n_t = buffers_n[std::pair < std::string, unsigned int > (c, n)];
    buffers_n_t.resize(t + 1, NAN);
    std::vector < double >& buffers_t_n = buffers_t[std::pair < std::string, unsigned int > (c, t)];
    buffers_t_n.resize(n + 1, NAN);
    Nsup[c] = new_channel || n >= Nsup.at(c) ? n + 1 : Nsup.at(c);
    Tsup[c] = new_channel || t >= Tsup.at(c) ? t + 1 : Tsup.at(c);
    return buffers_n_t[t] = buffers_t_n[n] = value;
  }
  void TransformBuffer::load(String filename)
  {
    wjson::Value data(aidesys::load (filename), true);
    load(aidesys::path(filename), data);
  }
  void TransformBuffer::load(String name, JSON data)
  {
    if(data.isArray()) {
      if(data.at(0).isArray()) {
        for(unsigned int n = 0; n < data.length(); n++) {
          if(data.at(n).isArray()) {
            for(unsigned int t = 0; t < data.at(n).length(); t++) {
              add(name, n, t, data.at(n).get(t, 0.0));
            }
          } else {
            add(name, n, 0, data.get(n, 0.0));
          }
        }
      } else {
        for(unsigned int t = 0; t < data.length(); t++) {
          add(name, 0, t, data.get(t, 0.0));
        }
      }
    } else if(data.isRecord()) {
      for(auto it = data.getNames().cbegin(); it != data.getNames().cend(); ++it) {
        load(*it, data.at(*it));
      }
    } else {
      add(name, 0, 0, data.get(0.0));
    }
  }
  void TransformBuffer::clear(String c)
  {
    if(c == "") {
      channels.clear();
      buffers_t.clear();
      buffers_n.clear();
      Nsup.clear();
      Tsup.clear();
    } else {
      for(unsigned int n = 0; n < Nsup.at(c); n++) {
        buffers_t.erase(std::pair < std::string, unsigned int > (c, n));
      }
      for(unsigned int t = 0; t < Tsup.at(c); t++) {
        buffers_n.erase(std::pair < std::string, unsigned int > (c, t));
      }
      Nsup.erase(c);
      Tsup.erase(c);
      channels.erase(c);
    }
  }
  double TransformBuffer::get(String c, unsigned int n, unsigned int t) const
  {
    auto it = buffers_n.find(std::pair < std::string, unsigned int > (c, n));
    return it == buffers_n.cend() || it->second.size() >= t ? NAN : it->second.at(t);
  }
  const std::vector < double >& TransformBuffer::getUnitSlice(String c, unsigned int n) const {
    auto it = buffers_n.find(std::pair < std::string, unsigned int > (c, n));
    return it == buffers_n.cend() ? empty : it->second;
  }
  const std::vector < double >& TransformBuffer::getTimeSlice(String c, unsigned int t) const {
    auto it = buffers_t.find(std::pair < std::string, unsigned int > (c, t));
    return it == buffers_t.cend() ? empty : it->second;
  }
  const std::vector < double >& TransformBuffer::getSamples(String c) const {
    static std::vector < double > all;
    all.clear();
    for(auto it = buffers_n.cbegin(); it != buffers_n.cend(); it++) {
      if(it->first.first == c) {
        all.insert(all.end(), it->second.cbegin(), it->second.cend());
      }
    }
    return all;
  }
  JSON TransformBuffer::getChannels() const
  {
    static wjson::Value result;
    result.clear();
    for(auto it = channels.cbegin(); it != channels.cend(); it++) {
      wjson::Value sizes;
      sizes.set("N", Nsup.at(*it));
      sizes.set("T", Tsup.at(*it));
      result.set(wjson::quote(*it), sizes);
    }
    return result;
  }
  std::string TransformBuffer::asString() const
  {
    return "{ TransformBuffer: {" + getChannels().asString() + "\n  } \n}";
  }
}
