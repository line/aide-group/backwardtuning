#include "QuadLinOutputWeightAdjustment.hpp"
#include "std.hpp"

namespace backwardtuning {
  QuadLinOutputWeightAdjustment::QuadLinOutputWeightAdjustment(JSON parameters) : Transform(parameters), WeightAdjustment(parameters), OutputWeightAdjustment(parameters)
  {
    q.setWo(w_o = parameters.get("w_o", 0.0));
  }
  std::string QuadLinOutputWeightAdjustment::asString() const
  {
    return aidesys::echo("QuadLinOutputWeightAdjustment: { " + OutputWeightAdjustment::asString() + " w_o: %g }", w_o);
  }
  double QuadLinOutputWeightAdjustment::c() const
  {
    double r = 0;
    for(unsigned int t = 0; t < T; t++) {
      for(unsigned int n = 0; n < No; n++) {
        r += q.f(getF(n, t) - o(n, t));
      }
    }
    return r;
  }
}
