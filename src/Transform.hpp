#ifndef __backwardtuning_Transform__
#define __backwardtuning_Transform__

#include <math.h>
#include "Value.hpp"

namespace backwardtuning {
  /**
   * @class Transform
   * @description Defines a general discrete time parameterized dynamical system.
   * - The `f(·)` method has to be implemented, as made explicit in the [equation summary](./derivaion.pdf)`.
   * - The forward state values are calculated, updated if needed and buffered for `0 <= n < N` and `0 <= t < T`.
   * - The parameters (or weights) are buffered at this level.
   * - By contract, connectivity is fixed and initializes in the constructor, variable connectivity is implemented via `0` value weights.
   * - By contract, weights are initialized in the constructor, their default value is `0`.
   * @param {JSON} parameters Object parameter:
   * - `N` Number of state units.
   * - `T` Time window step count.
   * - `K` Number of parameters.
   * - `Wstdev [= 0]` Weights normal random initial value standard-deviation, using [setWeights](#setWeights).
   * @throws illegal-argument exception if some parameter is ill-defined.
   */
  class Transform {
    friend class TransformObserver;
protected:
    // Object parameters
    wjson::Value parameters;
    // Transform buffers fvalues[n + N * t] and weights[k]
    double *fvalues, *weights;
    // This flag is to be set to true by derived classes if state values or weights are updated
    bool fmodified = true;
    // Transform dimensions
    unsigned int N, T, K;
    // Forward simulation, it is automatically called if required
    void forward();
public:
    Transform(JSON parameters);
    Transform(const Transform& transform);
    virtual ~Transform();

    /**
     * @function init
     * @memberof Transform
     * @instance
     * @abstract
     * @protected
     * @description Called at the beginning of the object construction to allow a programmatic determination of the parameters.
     * - This virtual routine can be overwritten by subclasses to set the transform architecture, such as connections,
     * @param {JSON} parameters The in/out parameters values.
     */
    void init(wjson::Value& parameters)
    {
    }
    /**
     * @function getParameter
     * @memberof Transform
     * @instance
     * @description Returns the object parameters.
     * @return {JSON} The parameters.
     */
    JSON getParameters() const
    {
      return parameters;
    }
    /**
     * @function asString
     * @memberof Transform
     * @instance
     * @description Returns object parameters as a parsable weak json string.
     * @return {string} The string.
     */
    virtual std::string asString() const;

    /**
     * @function f
     * @memberof Transform
     * @instance
     * @abstract
     * @description Implements the dynamic equation.
     * - By contract, out of bounds values are set to 0.
     * @param {uint} n Unit index.
     * @param {uint} t Current time.
     * @return {double} The corresponding value.
     */
    virtual double f(unsigned int n, unsigned int t) const;

    /**
     * @function getF
     * @memberof Transform
     * @instance
     * @description Returns the current forward value.
     * @param {uint} n Unit index.
     * @param {uint} t Current time.
     * @return {double} The corresponding value, or `0` if out of bound.
     */
    double getF(unsigned int n, unsigned int t) const
    {
      if(fmodified) {
        const_cast < Transform & > (*this).forward();
      }
      return N <= n || T <= t ? 0 : fvalues[n + N * t];
    }
    /**
     * @function getFValues
     * @memberof Transform
     * @instance
     * @description Returns the current forward values.
     * @return {Array} A 1D `double[N * T]` array with `x_n(t) = forward_values[n + N * t]`.
     */
    const double *getFValues() const
    {
      if(fmodified) {
        const_cast < Transform & > (*this).forward();
      }
      return fvalues;
    }
    /**
     * @function getW
     * @memberof Transform
     * @instance
     * @description Returns the current parameter value.
     * @param {uint} k Parameter index.
     * @return {double} The corresponding value, or `NAN` if out of bound.
     */
    double getW(unsigned int k) const
    {
      return K <= k || weights == NULL ? NAN : weights[k];
    }
    /**
     * @function getWeights
     * @memberof Transform
     * @instance
     * @description Returns the weight values
     * @return {Array} A `double[K]` array with the weight values.
     */
    const double *getWeights() const
    {
      return weights;
    }
    /**
     * @function setW
     * @memberof Transform
     * @instance
     * @description Sets the current parameter value.
     * @param {uint} k Parameter index.
     * @param {double} w The corresponding value.
     */
    virtual void setW(unsigned int k, double w)
    {
      if(k < K && weights != NULL) {
        fmodified = true;
        weights[k] = w;
      }
    }
    /**
     * @function setWeights
     * @memberof Transform
     * @instance
     * @description Randomly initializes the current parameter's values.
     * - Draws a normal distribution of zero mean and a given standard-deviation
     * @param {double} [Wstdev = 0] The standard-deviation, by default use `stdev = 1/K`
     *   - If `> 0`, weights are down a normal distribution of zero mean and a `Wstdev` standard-deviation.
     *   - If `-1`, uses the Xavier initialization value: `Wstdev = 1/N^(1/2)`.
     *   - If `-2`, uses the Brian initialization value: `Wstdev = 1/K`.
     *   - If `0` or any other negative value, weights are set to 0.
     */
    void setWeights(double Wstdev = 0);

    /**
     * @function setWeights
     * @memberof Transform
     * @instance
     * @description Initializes the current parameter's values from another transform or array.
     * - By contract the source transform must have compatible structure and number of parameters, otherwise:
     *  - If the source are more parameters, only the `K` 1st weights are considered.
     *  - If the source are less parameters, only these parameters are considered as the 1st parameters.
     * @param {Transform|Array} source The source transform.
     */
    void setWeights(const Transform& source);
    void setWeights(const double *W);

    /**
     * @function getWeightsDistance
     * @memberof Transform
     * @instance
     * @description Returns the distance between this weight vector and another source weight vector.
     * - By contract the source system must have compatible structure and number of parameters.
     * @param {Transform} source The source transform.
     * @param {char} [norm='2'] The chosen norm:
     * - '0' : The max norm.
     * - '1' : The L1 norm.
     * - '2' : The L2 norm.
     * @param {bool} [relative=false] If true calculates the relative distance : `||this - source|| / ||source||`.
     * @return {double} The corresponding value.
     */
    double getWeightsDistance(const Transform& source, char norm = '2', bool relative = false) const;
  };
}
#endif
