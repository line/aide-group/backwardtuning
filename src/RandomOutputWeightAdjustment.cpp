#include "RandomOutputWeightAdjustment.hpp"
#include "random.hpp"

namespace backwardtuning {
  RandomOutputWeightAdjustment::RandomOutputWeightAdjustment(JSON parameters) : Transform(parameters), WeightAdjustment(parameters), OutputWeightAdjustment(parameters), BufferedOutputWeightAdjustment(parameters), transform(parameters)
  {
    unique = parameters.get("unique", false);
  }
  std::string RandomOutputWeightAdjustment::asString() const
  {
    return "RandomOutputWeightAdjustment: { " + OutputWeightAdjustment::asString() + " unique: " + (unique ? "true" : "false") + " }";
  }
}
