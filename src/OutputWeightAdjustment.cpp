#include "OutputWeightAdjustment.hpp"
#include <math.h>
#include "std.hpp"

namespace backwardtuning {
  OutputWeightAdjustment::OutputWeightAdjustment(JSON parameters) : Transform(parameters), WeightAdjustment(parameters)
  {}
  std::string OutputWeightAdjustment::asString() const
  {
    return "OutputWeightAdjustment: { " + WeightAdjustment::asString() + " }";
  }
  double OutputWeightAdjustment::o(unsigned int n, unsigned int t) const
  {
    aidesys::alert(true, "illegal-state", "in backwardtuning::OutputWeightAdjustment::o(n,t), this virtual method must be overridden");
    return NAN;
  }
}
