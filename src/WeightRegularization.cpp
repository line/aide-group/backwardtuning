#include "WeightRegularization.hpp"

namespace backwardtuning {
  WeightRegularization::WeightRegularization(JSON parameters)  : Transform(parameters)
  {
    upsilon = upsilon0 = parameters.get("upsilon0", 1.0);
    aidesys::alert(upsilon0 <= 0, "illegal-argument", "in backwardtuning::WeightRegularization::setParameters upsilon = %g must be in ]0, +oo]", upsilon0);
  }
  std::string WeightRegularization::asString() const
  {
    return aidesys::echo("WeightRegularization: { upsilon: %g upsilon0: %g }", upsilon, upsilon);
  }
  double WeightRegularization::l(unsigned int k) const
  {
    aidesys::alert(true, "illegal-state", "in backwardtuning::WeightRegularization::l(k), this virtual method must be overridden");
    return NAN;
  }
}
