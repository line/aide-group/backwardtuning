@aideAPI

### This only an alpha version, better contact us to discuss, what it is and how to use it :)

 We propose here a re-implementation of backward tuning as developped and descrined in [(vieville et al 2017)](https://inria.hal.science/INRIA-RRRT/hal-01610735v1) allowing to adjust the parameters of a vectorial recurrent discrete-time dynamical system.
 
 Demonstrations:
 - [Visualisation of a quadlin sigmoid transform adjustment on random output](./sigmoidtransformdemo/master.html)
 - [Visualisation of a quadlin sigmoid transform adjustment on a mirror transform](./sigmoidtransformdemo/servant.html)
 - [Visualisation of a quadlin sigmoid transform adjustment with hidden units](./sigmoidtransformdemo/sigmoid2.html)
 - [Visualisation of a simple reservoir transform simulation](./reservoirtransformdemo/dummy_task.html)
 
 
@slides backwardtuning.pdf

## Implementation class hierarchy 

![implementation class hierarchy](./class-hierarchy.png)

