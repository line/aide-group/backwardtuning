#ifndef __backwardtuning_WeightRegularization__
#define __backwardtuning_WeightRegularization__

#include "Value.hpp"
#include "Transform.hpp"

namespace backwardtuning {
  /**
   * @class WeightRegularization
   * @description Defines the weight adjustment interface.
   * - The local weight quadratic factor `l(·)` method, related to minimizing <br>
   * `… + 1/upsilon |W - W^|^2_(l(·)^-1)` <br>
   * as made explicit in the [equation summary](./derivation.pdf)`, has to be implemented.
   * @extends Transform
   * @param {JSON} parameters Object parameter:
   * - `upsilon0 [= 1.0]` Parameter iterative initial gain, `upsilon0 > 0`.
   */
  class WeightRegularization: virtual public Transform {
    double upsilon0;
public:
    WeightRegularization(const WeightRegularization& transform) = delete;
    void operator = (const WeightRegularization&) = delete;
    WeightRegularization(JSON parameters);
    virtual ~WeightRegularization() {}
    virtual std::string asString() const;

    /**
     * @function l
     * @memberof WeightRegularization
     * @instance
     * @abstract
     * @description Implements the parameter weight factor.
     * @param {uint} k Parameter index.
     * @return {double} The corresponding value.
     */
    virtual double l(unsigned int k) const;

    /**
     * @member {double} upsilon
     * @memberof WeightRegularization
     * @static
     * @description Weigh adjustment gain, the initial value is adjusted during the weight adjustment process.
     */
    double upsilon = 1;
  };
}
#endif
